/* LLB_Thermal_properties.cpp
 *  Created on: 13 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include "math.h"

#include "../../hdr/Structures.hpp"

// Fits are obtained from "Dynamic response of the magnetisation to picosecond heat pulses" Natalia Kazantseva 2008

int LLB_Thermal_Properties(const int Num_Grains, const Grain_t Grain, LLB_t*LLB){
	/* The fit used provides Ms*Chi in SI units. --- Ms (FePt) = 1.04778546560508e+6.
	 * To obtain Chi (in SI) the result is multiplied by 1/Ms (FePt), in this case 9.54393845712027e-7.
	 * In order to obtain Chi in CGS, the value is multiplied by 1e-4. This converts from 1/T to 1/Oe. */
	for(int grain_in_system=0;grain_in_system<Num_Grains;++grain_in_system){

        // Fit Parameters :- All Parameters are pre-multiplied by 1e-7 (reduces computational errors).
       double a0_PARA = 1.21e-3,  a1_PARA = -2.2e-7,   a3_PARA = 1.95e-13,
        	  a4_PARA = -1.3e-17, a6_PARA = -4.00e-23, a9_PARA = -6.51e-32,
        	  b0_PARA = 2.12e-3;
       double a0_PERP = 2.11e-3,  a1_PERP = 1.10e-1,   a2_PERP = -8.55e-1, a3_PERP = 3.42,
    		  a4_PERP = -7.85,    a5_PERP = 1.03e+1,   a6_PERP = -6.86e-1, a7_PERP = 7.97e-1,
     		  a8_PERP = 1.54,     a9_PERP = -6.27e-1,
     		  b0_PERP = 4.85e-3;

       double Temperature = Grain.Temp[grain_in_system];
       double Tc = Grain.Tc[grain_in_system];

		if(Temperature<Tc){
			LLB->Chi_para[grain_in_system] = a0_PARA * (Tc/ (4*PI*(Tc-Temperature)) )
										  + a1_PARA * (Tc-Temperature)
										  + a3_PARA * pow((Tc-Temperature),3.0)
										  + a4_PARA * pow((Tc-Temperature),4.0)
			  	  	  	  	  	  	  	  + a6_PARA * pow((Tc-Temperature),6.0)
			  	  	  	  	  	  	  	  + a9_PARA * pow((Tc-Temperature),9.0);
		}
		else{LLB->Chi_para[grain_in_system] = b0_PARA * (Tc/ (4.0*PI*(Temperature-Tc)) );}
		// Chi_para must not be a negative value.
		if(LLB->Chi_para[grain_in_system]<0){LLB->Chi_para[grain_in_system] -= 1.1*LLB->Chi_para[grain_in_system];}
		LLB->Chi_para[grain_in_system] *= 9.54393845712027e-4;

      	if(Temperature<1.068*Tc){
			LLB->Chi_perp[grain_in_system] = a0_PERP
										  + a1_PERP*(1.0 - (Temperature/(1.068*Tc)))
										  + a2_PERP*pow((1.0 - (Temperature/(1.068*Tc))),2.0)
			  	  	  	  	  	  	  	  + a3_PERP*pow((1.0 - (Temperature/(1.068*Tc))),3.0)
			  	  	  	  	  	  	  	  + a4_PERP*pow((1.0 - (Temperature/(1.068*Tc))),4.0)
			  	  	  	  	  	  	  	  + a5_PERP*pow((1.0 - (Temperature/(1.068*Tc))),5.0)
			  	  	  	  	  	  	  	  + a6_PERP*pow((1.0 - (Temperature/(1.068*Tc))),6.0)
			  	  	  	  	  	  	  	  + a7_PERP*pow((1.0 - (Temperature/(1.068*Tc))),7.0)
			  	  	  	  	  	  	  	  + a8_PERP*pow((1.0 - (Temperature/(1.068*Tc))),8.0)
			  	  	  	  	  	  	  	  + a9_PERP*pow((1.0 - (Temperature/(1.068*Tc))),9.0);
		}
		else{LLB->Chi_perp[grain_in_system]= b0_PERP * (Tc/ (4.0*PI*(Temperature-Tc)) );}
      	LLB->Chi_perp[grain_in_system] *= 9.54393845712027e-4;

	}
	return 0;
}


