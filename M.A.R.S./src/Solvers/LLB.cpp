/* LLB.cpp
 *  Created on: 13 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include "math.h"
#include <random>

#include <iomanip>

#include "../../hdr/Solvers/LLB_Thermal_Properties.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/RNG/Random.hpp"

int LL_B(const int Num_Grains,const int Num_Layers,const Interaction_t Interac,const Voronoi_t VORO,LLB_t*LLB,Grain_t*Grain){

	// Set total number of grains in system
	double Tot_Grains = Num_Grains*Num_Layers;

	// Local parameters
	double Offset;
	double MdotE, MdotH, m_sqrd, m_MAG_SQ,
		   m_X_dummy, m_Y_dummy, m_Z_dummy, m_X_dummy_2, m_Y_dummy_2 , m_Z_dummy_2,
	       H_eff_X_dummy, H_eff_Y_dummy, H_eff_Z_dummy,
	       Alpha_PARA_dummy, Alpha_PERP_dummy,
		   RNGperp_X_dummy, RNGperp_Y_dummy, RNGperp_Z_dummy,
		   RNGpara_X_dummy, RNGpara_Y_dummy, RNGpara_Z_dummy,
		   Callen_power;
	Vec3 MxH, MxMxH, MxRNG_PERP, MxMxRNG_PERP;
	std::vector<Vec3> M_initial (Tot_Grains), dmdt1 (Tot_Grains), dmdt2 (Tot_Grains),
					  RNG_PARA (Tot_Grains), RNG_PERP (Tot_Grains),
					  RNG_PARA_NUM (Tot_Grains), RNG_PERP_NUM (Tot_Grains),
					  H_eff (Tot_Grains), H_magneto (Tot_Grains), H_exchange (Tot_Grains);
	std::vector<double>  Internal_Exchange_field (Tot_Grains), H_ani (Tot_Grains);
	int grain_in_layer,grain_in_system;

	// Resize LLB vectors
	LLB->Chi_para.resize(Tot_Grains);
	LLB->Chi_perp.resize(Tot_Grains);
	LLB->Alpha_PARA.resize(Tot_Grains);
	LLB->Alpha_PERP.resize(Tot_Grains);
	LLB->m_EQ.resize(Tot_Grains);

	//-- READ IN MAGNETISATION DATA
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		M_initial[grain_in_system].x = Grain->m[grain_in_system].x;
		M_initial[grain_in_system].y = Grain->m[grain_in_system].y;
		M_initial[grain_in_system].z = Grain->m[grain_in_system].z;
		// Save random numbers
		RNG_PARA_NUM[grain_in_system].x = mtrandom::gaussian();
		RNG_PARA_NUM[grain_in_system].y = mtrandom::gaussian();
		RNG_PARA_NUM[grain_in_system].z = mtrandom::gaussian();
		RNG_PERP_NUM[grain_in_system].x = mtrandom::gaussian();
		RNG_PERP_NUM[grain_in_system].y = mtrandom::gaussian();
		RNG_PERP_NUM[grain_in_system].z = mtrandom::gaussian();
	}

// DOES THIS CAUSE ANY ISSUES FOR SYSTEMS WITH A DISTRIBUTIONS OF Tc?
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		// Ensure T != Tc as solution will be infinity.
		if(Grain->Temp[grain_in_system]==Grain->Tc[grain_in_system]){
			std::cout << "\nWARNING:: Temp=Tc for grain '" << grain_in_system << "' increasing grain temperature by 0.1K." << std::endl;
			Grain->Temp[grain_in_system]+=0.1;
	}	}

//################################################################# DETERMINE TEMPERATURE DEPENDENT PARAMETERS ##########################################################//
	LLB_Thermal_Properties(Tot_Grains, *Grain, LLB);

	for(int Layer=0;Layer<Num_Layers;++Layer){
		Offset = Num_Grains*Layer;
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			grain_in_system = grain_in_layer+Offset;

			double Temperature = Grain->Temp[grain_in_system];
			double Tc = Grain->Tc[grain_in_system];
			if(Temperature<=Tc){
				LLB->m_EQ[grain_in_system] = pow(1.0-(Temperature/Tc),Grain->Crit_exp[grain_in_system]); // PAPER
				LLB->Alpha_PARA[grain_in_system] = LLB->Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
				LLB->Alpha_PERP[grain_in_system] = LLB->Alpha[Layer]*(1.0-(Temperature/(3.0*Tc)));
			}
			else{
				LLB->m_EQ[grain_in_system] = 0.0;
				LLB->Alpha_PARA[grain_in_system] = LLB->Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
				LLB->Alpha_PERP[grain_in_system] = LLB->Alpha_PARA[grain_in_system];
	}	}	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE STOCHASTIC TERMS ######################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){

		double Temperature = Grain->Temp[grain_in_system];
		double Tc = Grain->Tc[grain_in_system];
		double m_EQ_dummy = LLB->m_EQ[grain_in_system];
		double Chi_PARA_dummy = LLB->Chi_para[grain_in_system];
		double Alpha_PARA_dummy = LLB->Alpha_PARA[grain_in_system];
		double Alpha_PERP_dummy = LLB->Alpha_PERP[grain_in_system];

		m_X_dummy = Grain->m[grain_in_system].x;
		m_Y_dummy = Grain->m[grain_in_system].y;
		m_Z_dummy = Grain->m[grain_in_system].z;

		double RNG_PARA_MAG = sqrt( (2.0*KB*Temperature*Alpha_PARA_dummy) / (LLB->Gamma*Grain->Ms[grain_in_system]*(Grain->Vol[grain_in_system]*1e-21)*LLB->dt ) );
		RNG_PARA[grain_in_system].x = RNG_PARA_NUM[grain_in_system].x*RNG_PARA_MAG;
		RNG_PARA[grain_in_system].y = RNG_PARA_NUM[grain_in_system].y*RNG_PARA_MAG;
		RNG_PARA[grain_in_system].z = RNG_PARA_NUM[grain_in_system].z*RNG_PARA_MAG;
		double RNG_PERP_MAG = sqrt( (2.0*KB*Temperature*(Alpha_PERP_dummy-Alpha_PARA_dummy)) / (LLB->Gamma*Grain->Ms[grain_in_system]*(Grain->Vol[grain_in_system]*1e-21)*LLB->dt*(Alpha_PERP_dummy*Alpha_PERP_dummy)) );
		RNG_PERP[grain_in_system].x = RNG_PERP_NUM[grain_in_system].x*RNG_PERP_MAG;
		RNG_PERP[grain_in_system].y = RNG_PERP_NUM[grain_in_system].y*RNG_PERP_MAG;
		RNG_PERP[grain_in_system].z = RNG_PERP_NUM[grain_in_system].z*RNG_PERP_MAG;

		m_MAG_SQ = m_X_dummy*m_X_dummy + m_Y_dummy*m_Y_dummy + m_Z_dummy*m_Z_dummy;

		H_ani[grain_in_system] = ((2.0*Grain->K[grain_in_system])/Grain->Ms[grain_in_system]) * pow(m_EQ_dummy,Grain->Callen_power[grain_in_system]-1.0);

		if (Temperature <= Tc){Internal_Exchange_field[grain_in_system] = (1.0/(2.0*Chi_PARA_dummy))*(1.0-(m_MAG_SQ/(m_EQ_dummy*m_EQ_dummy)));}
		else{Internal_Exchange_field[grain_in_system] = (-1.0/Chi_PARA_dummy)*(1.0+((3.0*Tc)/(5.0*(Temperature-Tc)))*m_MAG_SQ);}
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		MdotE = Grain->m[grain_in_system].x*Grain->Easy_axis[grain_in_system].x + Grain->m[grain_in_system].y*Grain->Easy_axis[grain_in_system].y + Grain->m[grain_in_system].z*Grain->Easy_axis[grain_in_system].z;
		H_eff[grain_in_system].x = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].x + Grain->H_appl[grain_in_system].x + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].x;
		H_eff[grain_in_system].y = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].y + Grain->H_appl[grain_in_system].y + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].y;
		H_eff[grain_in_system].z = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].z + Grain->H_appl[grain_in_system].z + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].z;
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_magneto[grain_in_system].x = H_magneto[grain_in_system].y = H_magneto[grain_in_system].z =0.0;
		for(int neigh=0;neigh<Interac.Wxx[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];
			H_magneto[grain_in_system].x += Grain->Ms[grain_in_system]*(Interac.Wxx[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);

			H_magneto[grain_in_system].y += Grain->Ms[grain_in_system]*(Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);

			H_magneto[grain_in_system].z += Grain->Ms[grain_in_system]*(Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wzz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);
		}
		H_exchange[grain_in_system].x = H_exchange[grain_in_system].y = H_exchange[grain_in_system].z = 0.0;
		for(int neigh=0;neigh<Interac.H_exch_str[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
			H_exchange[grain_in_system].x += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID];
			H_exchange[grain_in_system].y += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID];
			H_exchange[grain_in_system].z += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID];
	}	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_eff[grain_in_system].x += H_magneto[grain_in_system].x + H_exchange[grain_in_system].x;
		H_eff[grain_in_system].y += H_magneto[grain_in_system].y + H_exchange[grain_in_system].y;
		H_eff[grain_in_system].z += H_magneto[grain_in_system].z + H_exchange[grain_in_system].z;
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### FIRST HEUN STEP ###########################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){

		m_X_dummy = Grain->m[grain_in_system].x;
		m_Y_dummy = Grain->m[grain_in_system].y;
		m_Z_dummy = Grain->m[grain_in_system].z;
		H_eff_X_dummy = H_eff[grain_in_system].x;
		H_eff_Y_dummy = H_eff[grain_in_system].y;
		H_eff_Z_dummy = H_eff[grain_in_system].z;
		Alpha_PARA_dummy = LLB->Alpha_PARA[grain_in_system];
		Alpha_PERP_dummy = LLB->Alpha_PERP[grain_in_system];
		RNGpara_X_dummy = RNG_PARA[grain_in_system].x;
		RNGpara_Y_dummy = RNG_PARA[grain_in_system].y;
		RNGpara_Z_dummy = RNG_PARA[grain_in_system].z;
		RNGperp_X_dummy = RNG_PERP[grain_in_system].x;
		RNGperp_Y_dummy = RNG_PERP[grain_in_system].y;
		RNGperp_Z_dummy = RNG_PERP[grain_in_system].z;

		m_sqrd = m_X_dummy*m_X_dummy + m_Y_dummy*m_Y_dummy + m_Z_dummy*m_Z_dummy;
		MdotH = m_X_dummy*H_eff_X_dummy + m_Y_dummy*H_eff_Y_dummy + m_Z_dummy*H_eff_Z_dummy;

		dmdt1[grain_in_system].x = LLB->Gamma*(-( m_Y_dummy*H_eff_Z_dummy - m_Z_dummy*H_eff_Y_dummy )
										      +(Alpha_PARA_dummy/m_sqrd)*( m_X_dummy*MdotH )
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Y_dummy*(m_X_dummy*H_eff_Y_dummy-m_Y_dummy*H_eff_X_dummy)-m_Z_dummy*(m_Z_dummy*H_eff_X_dummy-m_X_dummy*H_eff_Z_dummy))
										      + RNGpara_X_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Y_dummy*(m_X_dummy*RNGperp_Y_dummy-m_Y_dummy*RNGperp_X_dummy)-m_Z_dummy*(m_Z_dummy*RNGperp_X_dummy-m_X_dummy*RNGperp_Z_dummy)));
		dmdt1[grain_in_system].y = LLB->Gamma*(-( m_Z_dummy*H_eff_X_dummy - m_X_dummy*H_eff_Z_dummy )
										      +(Alpha_PARA_dummy/m_sqrd)*( m_Y_dummy*MdotH )
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Z_dummy*(m_Y_dummy*H_eff_Z_dummy-m_Z_dummy*H_eff_Y_dummy)-m_X_dummy*(m_X_dummy*H_eff_Y_dummy-m_Y_dummy*H_eff_X_dummy))
										      + RNGpara_Y_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Z_dummy*(m_Y_dummy*RNGperp_Z_dummy-m_Z_dummy*RNGperp_Y_dummy)-m_X_dummy*(m_X_dummy*RNGperp_Y_dummy-m_Y_dummy*RNGperp_X_dummy)));
		dmdt1[grain_in_system].z = LLB->Gamma*(-( m_X_dummy*H_eff_Y_dummy - m_Y_dummy*H_eff_X_dummy )
								              +(Alpha_PARA_dummy/m_sqrd)*( m_Z_dummy*MdotH )
								              -(Alpha_PERP_dummy/m_sqrd)*( m_X_dummy*(m_Z_dummy*H_eff_X_dummy-m_X_dummy*H_eff_Z_dummy)-m_Y_dummy*(m_Y_dummy*H_eff_Z_dummy-m_Z_dummy*H_eff_Y_dummy))
								              + RNGpara_Z_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_X_dummy*(m_Z_dummy*RNGperp_X_dummy-m_X_dummy*RNGperp_Z_dummy)-m_Y_dummy*(m_Y_dummy*RNGperp_Z_dummy-m_Z_dummy*RNGperp_Y_dummy)));

		Grain->m[grain_in_system].x = m_X_dummy + dmdt1[grain_in_system].x*LLB->dt;
		Grain->m[grain_in_system].y = m_Y_dummy + dmdt1[grain_in_system].y*LLB->dt;
		Grain->m[grain_in_system].z = m_Z_dummy + dmdt1[grain_in_system].z*LLB->dt;
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################### REDETERMINE THERMALLY DEPENDENT TERMS #####################################################################//

	for(int Layer=0;Layer<Num_Layers;++Layer){
		Offset = Num_Grains*Layer;
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			grain_in_system = grain_in_layer+Offset;
			double Temperature = Grain->Temp[grain_in_system];
			double Tc = Grain->Tc[grain_in_system];
			if(Temperature<=Tc){
				LLB->m_EQ[grain_in_system] = pow(1.0-(Temperature/Tc),Grain->Crit_exp[grain_in_system]); // PAPER
				LLB->Alpha_PARA[grain_in_system] = LLB->Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
				LLB->Alpha_PERP[grain_in_system] = LLB->Alpha[Layer]*(1.0-(Temperature/(3.0*Tc)));
			}
			else{
				LLB->m_EQ[grain_in_system] = 0.0;
				LLB->Alpha_PARA[grain_in_system] = LLB->Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
				LLB->Alpha_PERP[grain_in_system] = LLB->Alpha_PARA[grain_in_system];
	}	}	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//################################################################### REDETERMINE STOCHASTIC TERMS ######################################################################//

	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){

		double Temperature = Grain->Temp[grain_in_system];
		double Tc = Grain->Tc[grain_in_system];
		double m_EQ_dummy = LLB->m_EQ[grain_in_system];
		double Chi_PARA_dummy = LLB->Chi_para[grain_in_system];
		double Alpha_PARA_dummy = LLB->Alpha_PARA[grain_in_system];
		double Alpha_PERP_dummy = LLB->Alpha_PERP[grain_in_system];

		m_X_dummy_2 = Grain->m[grain_in_system].x;
		m_Y_dummy_2 = Grain->m[grain_in_system].y;
		m_Z_dummy_2 = Grain->m[grain_in_system].z;

		double RNG_PARA_MAG = sqrt( (2.0*KB*Temperature*Alpha_PARA_dummy) / (LLB->Gamma*Grain->Ms[grain_in_system]*(Grain->Vol[grain_in_system]*1e-21)*LLB->dt ) );
		RNG_PARA[grain_in_system].x = RNG_PARA_NUM[grain_in_system].x*RNG_PARA_MAG;
		RNG_PARA[grain_in_system].y = RNG_PARA_NUM[grain_in_system].y*RNG_PARA_MAG;
		RNG_PARA[grain_in_system].z = RNG_PARA_NUM[grain_in_system].z*RNG_PARA_MAG;
		double RNG_PERP_MAG = sqrt( (2.0*KB*Temperature*(Alpha_PERP_dummy-Alpha_PARA_dummy)) / (LLB->Gamma*Grain->Ms[grain_in_system]*(Grain->Vol[grain_in_system]*1e-21)*LLB->dt*(Alpha_PERP_dummy*Alpha_PERP_dummy)) );
		RNG_PERP[grain_in_system].x = RNG_PERP_NUM[grain_in_system].x*RNG_PERP_MAG;
		RNG_PERP[grain_in_system].y = RNG_PERP_NUM[grain_in_system].y*RNG_PERP_MAG;
		RNG_PERP[grain_in_system].z = RNG_PERP_NUM[grain_in_system].z*RNG_PERP_MAG;

		m_MAG_SQ = m_X_dummy_2*m_X_dummy_2 + m_Y_dummy_2*m_Y_dummy_2 + m_Z_dummy_2*m_Z_dummy_2;

		H_ani[grain_in_system] = ((2.0*Grain->K[grain_in_system])/Grain->Ms[grain_in_system]) * pow(m_EQ_dummy,Grain->Callen_power[grain_in_system]-1.0);

		if (Temperature <= Tc){Internal_Exchange_field[grain_in_system] = (1.0/(2.0*Chi_PARA_dummy))*(1.0-(m_MAG_SQ/(m_EQ_dummy*m_EQ_dummy)));}
		else{Internal_Exchange_field[grain_in_system] = (-1.0/Chi_PARA_dummy)*(1.0+((3.0*Tc)/(5.0*(Temperature-Tc)))*m_MAG_SQ);}
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//################################################################### REDETERMINE EFFECTIVE H-FIELD #####################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		MdotE = Grain->m[grain_in_system].x*Grain->Easy_axis[grain_in_system].x + Grain->m[grain_in_system].y*Grain->Easy_axis[grain_in_system].y + Grain->m[grain_in_system].z*Grain->Easy_axis[grain_in_system].z;
		H_eff[grain_in_system].x = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].x + Grain->H_appl[grain_in_system].x + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].x;
		H_eff[grain_in_system].y = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].y + Grain->H_appl[grain_in_system].y + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].y;
		H_eff[grain_in_system].z = Internal_Exchange_field[grain_in_system]*Grain->m[grain_in_system].z + Grain->H_appl[grain_in_system].z + H_ani[grain_in_system] * MdotE * Grain->Easy_axis[grain_in_system].z;
	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_magneto[grain_in_system].x = H_magneto[grain_in_system].y = H_magneto[grain_in_system].z =0.0;
		for(int neigh=0;neigh<Interac.Wxx[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Magneto_neigh_list[grain_in_system][neigh];
			H_magneto[grain_in_system].x += Grain->Ms[grain_in_system]*(Interac.Wxx[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);

			H_magneto[grain_in_system].y += Grain->Ms[grain_in_system]*(Interac.Wxy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyy[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);

			H_magneto[grain_in_system].z += Grain->Ms[grain_in_system]*(Interac.Wxz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wyz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID]+
																		Interac.Wzz[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID]);
		}
		H_exchange[grain_in_system].x = H_exchange[grain_in_system].y = H_exchange[grain_in_system].z = 0.0;
		for(int neigh=0;neigh<Interac.H_exch_str[grain_in_system].size();neigh++){
			int NEIGHBOUR_ID = Interac.Exchange_neigh_list[grain_in_system][neigh];
			H_exchange[grain_in_system].x += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].x*LLB->m_EQ[NEIGHBOUR_ID];
			H_exchange[grain_in_system].y += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].y*LLB->m_EQ[NEIGHBOUR_ID];
			H_exchange[grain_in_system].z += Interac.H_exch_str[grain_in_system][neigh]*Grain->m[NEIGHBOUR_ID].z*LLB->m_EQ[NEIGHBOUR_ID];
	}	}
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){
		H_eff[grain_in_system].x += H_magneto[grain_in_system].x + H_exchange[grain_in_system].x;
		H_eff[grain_in_system].y += H_magneto[grain_in_system].y + H_exchange[grain_in_system].y;
		H_eff[grain_in_system].z += H_magneto[grain_in_system].z + H_exchange[grain_in_system].z;
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### SECOND HEUN STEP ##########################################################################//
	for(int grain_in_system=0;grain_in_system<Tot_Grains;++grain_in_system){

		m_X_dummy_2 = Grain->m[grain_in_system].x;
		m_Y_dummy_2 = Grain->m[grain_in_system].y;
		m_Z_dummy_2 = Grain->m[grain_in_system].z;
		H_eff_X_dummy = H_eff[grain_in_system].x;
		H_eff_Y_dummy = H_eff[grain_in_system].y;
		H_eff_Z_dummy = H_eff[grain_in_system].z;
		Alpha_PARA_dummy = LLB->Alpha_PARA[grain_in_system];
		Alpha_PERP_dummy = LLB->Alpha_PERP[grain_in_system];
		RNGpara_X_dummy = RNG_PARA[grain_in_system].x;
		RNGpara_Y_dummy = RNG_PARA[grain_in_system].y;
		RNGpara_Z_dummy = RNG_PARA[grain_in_system].z;
		RNGperp_X_dummy = RNG_PERP[grain_in_system].x;
		RNGperp_Y_dummy = RNG_PERP[grain_in_system].y;
		RNGperp_Z_dummy = RNG_PERP[grain_in_system].z;

		m_sqrd = m_X_dummy_2*m_X_dummy_2 + m_Y_dummy_2*m_Y_dummy_2 + m_Z_dummy_2*m_Z_dummy_2;
		MdotH = m_X_dummy_2*H_eff_X_dummy + m_Y_dummy_2*H_eff_Y_dummy + m_Z_dummy_2*H_eff_Z_dummy;

		dmdt2[grain_in_system].x = LLB->Gamma*(-( m_Y_dummy_2*H_eff_Z_dummy - m_Z_dummy_2*H_eff_Y_dummy )
										      +(Alpha_PARA_dummy/m_sqrd)*( m_X_dummy_2*MdotH )
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Y_dummy_2*(m_X_dummy_2*H_eff_Y_dummy-m_Y_dummy_2*H_eff_X_dummy)-m_Z_dummy_2*(m_Z_dummy_2*H_eff_X_dummy-m_X_dummy_2*H_eff_Z_dummy))
										      + RNGpara_X_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Y_dummy_2*(m_X_dummy_2*RNGperp_Y_dummy-m_Y_dummy_2*RNGperp_X_dummy)-m_Z_dummy_2*(m_Z_dummy_2*RNGperp_X_dummy-m_X_dummy_2*RNGperp_Z_dummy)));
		dmdt2[grain_in_system].y = LLB->Gamma*(-( m_Z_dummy_2*H_eff_X_dummy - m_X_dummy_2*H_eff_Z_dummy )
										      +(Alpha_PARA_dummy/m_sqrd)*( m_Y_dummy_2*MdotH )
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Z_dummy_2*(m_Y_dummy_2*H_eff_Z_dummy-m_Z_dummy_2*H_eff_Y_dummy)-m_X_dummy_2*(m_X_dummy_2*H_eff_Y_dummy-m_Y_dummy_2*H_eff_X_dummy))
										      + RNGpara_Y_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_Z_dummy_2*(m_Y_dummy_2*RNGperp_Z_dummy-m_Z_dummy_2*RNGperp_Y_dummy)-m_X_dummy_2*(m_X_dummy_2*RNGperp_Y_dummy-m_Y_dummy_2*RNGperp_X_dummy)));
		dmdt2[grain_in_system].z = LLB->Gamma*(-( m_X_dummy_2*H_eff_Y_dummy - m_Y_dummy_2*H_eff_X_dummy )
								              +(Alpha_PARA_dummy/m_sqrd)*( m_Z_dummy_2*MdotH )
								              -(Alpha_PERP_dummy/m_sqrd)*( m_X_dummy_2*(m_Z_dummy_2*H_eff_X_dummy-m_X_dummy_2*H_eff_Z_dummy)-m_Y_dummy_2*(m_Y_dummy_2*H_eff_Z_dummy-m_Z_dummy_2*H_eff_Y_dummy))
								              + RNGpara_Z_dummy
										      -(Alpha_PERP_dummy/m_sqrd)*( m_X_dummy_2*(m_Z_dummy_2*RNGperp_X_dummy-m_X_dummy_2*RNGperp_Z_dummy)-m_Y_dummy_2*(m_Y_dummy_2*RNGperp_Z_dummy-m_Z_dummy_2*RNGperp_Y_dummy)));

		Grain->m[grain_in_system].x = M_initial[grain_in_system].x + (dmdt1[grain_in_system].x+dmdt2[grain_in_system].x)*(LLB->dt*0.5);
		Grain->m[grain_in_system].y = M_initial[grain_in_system].y + (dmdt1[grain_in_system].y+dmdt2[grain_in_system].y)*(LLB->dt*0.5);
		Grain->m[grain_in_system].z = M_initial[grain_in_system].z + (dmdt1[grain_in_system].z+dmdt2[grain_in_system].z)*(LLB->dt*0.5);
	}

	return 0;
}
