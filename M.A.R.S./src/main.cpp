/* M.A.R.S. - Models of Advanced Recording Systems
 *  Created on: 23 Feb 2018
 *      Author: Samuel Ewan Rannala
 */

//---------------------------------------------//
#ifdef USEDEBUG
#define Debug(x) std::cout << x
#else
#define Debug(x)
#endif
//---------------------------------------------//

// System header files
#include <iostream>
#include <fstream>
#include <random>

// M.A.R.S specific header files.
#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/RNG/Random.hpp"
#include "../hdr/Tests/Test_LLG_analyt.hpp"
#include "../hdr/Tests/Test_LLG_hyst.hpp"
#include "../hdr/Tests/Test_LLG_boltz.hpp"
#include "../hdr/Tests/Test_LLB_mVt.hpp"
#include "../hdr/Tests/Test_KMC_SW.hpp"
#include "../hdr/Tests/Test_KMC_Hyst.hpp"
#include "../hdr/Globals.hpp"

#include  "../hdr/HAMR/HAMR.hpp"
#include "../hdr/HAMR/HAMR_localised_write.hpp"
#include "../hdr/HAMR/HAMR_Write_Data.hpp"
#include "../hdr/System_generate.hpp"
#include "../hdr/Sigma_Tc.hpp"

double PI, KB;
std::mt19937_64 Gen;

int main(){
	Debug("******Debugging enabled.******" << std::endl;);

	// Define constants.
	PI = 3.141592653589;
	KB = 1.3806485279e-16;

	// Read in data
	ConfigFile cfg("Input/MARS_input.cfg");

	// Obtain desired SEED and apply it to global generators
	int SEED = cfg.getValueOfKey<int>("sim:seed");
	Gen.seed(SEED);
	mtrandom::grnd.seed(SEED);

	// Determine desired simulation
	std::string Sim_Type = cfg.getValueOfKey<std::string>("sim:type");
	if(Sim_Type=="test"){
		std::cout << "Testing... \n" << std::flush;
		std::string Test_type = cfg.getValueOfKey<std::string>("test:type");

		if(Test_type=="llg-analytical"){LLG_analyt_test();}
		else if(Test_type=="llg-hysteresis"){LLG_hyst_test();}
		else if(Test_type=="llg-boltzmann"){LLG_boltz_test();}
		else if(Test_type=="llb-mvt"){LLB_mVt_test();}
		else if(Test_type=="kmc_sw"){KMC_SW_test();}
		else if(Test_type=="kmc_hysteresis"){KMC_Hyst_test();}
		else{std::cout << "Unidentified test type" << std::endl;}
	}
	else{
		if(Sim_Type=="hamr"){HAMR(cfg);}
		else if(Sim_Type=="hamr_write_local"){HAMR_localised_write(cfg);}
		else if(Sim_Type=="hamr_write_data"){HAMR_write_data(cfg);}
		else if(Sim_Type=="system_gen"){Gen_sys(cfg);}
		else if(Sim_Type=="sigma_tc"){Sigma_Tc(cfg);}
	}
	return 0;
}

