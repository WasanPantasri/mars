/* LLB_import.cpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int LLB_import(const ConfigFile cfg, const std::vector<ConfigFile> Materials_config, const int Num_Layers, LLB_t*LLB){
	std::cout << "Assigning LLB parameters..." << std::endl;

	const std::string BoldCyanFont = "\033[1;36m";
	const std::string BoldYellowFont = "\033[1;33m";
	const std::string ResetFont = "\033[0m";

//####################Retrieve values from hash table####################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		LLB->Alpha.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Alpha"));
	}
	LLB->dt    = cfg.getValueOfKey<double>("llb:dt");

//####################Output parameter values to end-user####################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		std::cout << BoldCyanFont << "\tAlpha " << Layer+1 << "                                    = \t" << BoldYellowFont << LLB->Alpha[Layer] << " \t" << std::endl;
	}
	std::cout << BoldCyanFont << "\tdt                                         = \t" << BoldYellowFont << LLB->dt << " \t" << std::endl;
	std::cout << ResetFont << std::endl;

	return 0;
}


