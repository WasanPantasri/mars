/* Materials_import.cpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <cstring>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int Materials_import(const ConfigFile cfg, const int Num_Layers, std::vector<ConfigFile>*Material_Config, Material_t*Material){

	const std::string BoldCyanFont = "\033[1;36m", BoldYellowFont = "\033[1;33m",
			          BoldRedFont = "\033[1;4;31m", ResetFont = "\033[0m";
	std::vector<double> temp_mag, temp_EA;
	int Material_number;

	std::cout << "Assigning material parameters..." << std::endl;
	Material->Initial_mag.resize(Num_Layers); 	Material->Initial_anis.resize(Num_Layers);
	Material->z.push_back(0.0);


	for(int Layer=0;Layer<Num_Layers;++Layer){
		// Determine Material number
		Material_number = Layer+1;
		std::string key_prefix = "Mat" + std::to_string(Material_number) + ":";

		std::string FILE_LOC = "Input/Materials/" + cfg.getValueOfKey<std::string>(key_prefix+"File");
		ConfigFile MAT_FILE(FILE_LOC.c_str());
		Material_Config->push_back(MAT_FILE); // Need the config file for future use.

		// Import all variables
		Material->Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:type"));

		Material->Mag_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:Initial_mag_type"));
		if(Material->Mag_Type.back() != "assigned" && Material->Mag_Type.back() != "random"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Mag type. -> " << Material->Mag_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		if(Material->Mag_Type.back()=="assigned"){
			temp_mag = MAT_FILE.getValueOfKey<std::vector<double>>("Mat:Initial_mag");
			Material->Initial_mag[Layer].x = temp_mag[0];
			Material->Initial_mag[Layer].y = temp_mag[1];
			Material->Initial_mag[Layer].z = temp_mag[2];
		}
		Material->Easy_axis_polar.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_polar"));
 		Material->Easy_axis_azimuth.push_back(MAT_FILE.getValueOfKey<double>("Mat:easy_axis_azimuth"));

 		Material->dz.push_back(MAT_FILE.getValueOfKey<double>("Mat:thickness"));
 		if(Layer>0){Material->z.push_back(Material->z[Layer-1]+(Material->dz[Layer-1]+Material->dz[Layer])*0.5);}
 		Material->Ms.push_back(MAT_FILE.getValueOfKey<double>("Mat:ms"));

		Material->K_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:k_Dist_type"));
		if(Material->K_Dist_Type.back() != "normal" && Material->K_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown K distribution type. -> " << Material->K_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->Avg_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_k"));
		Material->StdDev_K.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_k"));
		Material->Callen_power.push_back(MAT_FILE.getValueOfKey<double>("Mat:Callen_power"));

		Material->Tc_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:tc_dist_type"));
		if(Material->Tc_Dist_Type.back() != "normal" && Material->Tc_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown Tc distribution type. -> " << Material->Tc_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->Avg_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:avg_tc"));
		Material->StdDev_Tc.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_tc"));

		Material->J_Dist_Type.push_back(MAT_FILE.getValueOfKey<std::string>("Mat:j_dist_type"));
		if(Material->J_Dist_Type.back() != "normal" && Material->J_Dist_Type.back() != "log-normal"){
			std::cout << BoldRedFont << " MAT_FILE error: Material " << std::to_string(Material_number) << " Unknown J distribution type. -> " << Material->J_Dist_Type.back() << ResetFont << std::endl;
			exit (EXIT_FAILURE);
		}
		Material->StdDev_J.push_back(MAT_FILE.getValueOfKey<double>("Mat:stddev_j"));
		Material->H_sat.push_back(MAT_FILE.getValueOfKey<double>("Mat:h_sat"));
		Material->Anis_angle.push_back(MAT_FILE.getValueOfKey<double>("Mat:anis_angle"));
		Material->Crit_exp.push_back(MAT_FILE.getValueOfKey<double>("Mat:Critical_Exponent"));

		if(Num_Layers>1){
			Material->Hexch_str_out_plane_UP.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_UP"));
			if(Layer==0){
				Material->Hexch_str_out_plane_DOWN.push_back(0.0);
			}
			else if(Layer!=0){
				Material->Hexch_str_out_plane_DOWN.push_back(MAT_FILE.getValueOfKey<double>("Mat:Hexch_out_of_layer_DOWN"));
		}	}

	}
//####################Output parameter values to end-user####################//
	for(int Layer=0;Layer<Num_Layers;++Layer){
		Material_number = Layer+1;
		std::cout << "..............................Material " + std::to_string(Material_number) + ".............................." << std::endl;
		std::cout << BoldCyanFont << "\tType                                       = \t" << BoldYellowFont << Material->Type[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\tThickness                                  = \t" << BoldYellowFont << Material->dz[Layer] << " nm\t" << std::endl;
		std::cout << BoldCyanFont << "\tMs                                         = \t" << BoldYellowFont << Material->Ms[Layer] << " emu/cc\t" << std::endl;
		std::cout << BoldCyanFont << "\tK distribution type                        = \t" << BoldYellowFont << Material->K_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     Average K                             = \t" << BoldYellowFont << Material->Avg_K[Layer] << " erg/cc\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation K                  = \t" << BoldYellowFont << Material->StdDev_K[Layer] << " erg/cc\t" << std::endl;
		std::cout << BoldCyanFont << "\tTc distribution type                       = \t" << BoldYellowFont << Material->Tc_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     Average Tc                            = \t" << BoldYellowFont << Material->Avg_Tc[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation Tc                 = \t" << BoldYellowFont << Material->StdDev_Tc[Layer] << " K\t" << std::endl;
		std::cout << BoldCyanFont << "\tJ distribution type                        = \t" << BoldYellowFont << Material->J_Dist_Type[Layer]  << std::endl;
		std::cout << BoldCyanFont << "\t     H_Sat                                 = \t" << BoldYellowFont << Material->H_sat[Layer] << " Oe\t" << std::endl;
		std::cout << BoldCyanFont << "\t     Standard deviation J                  = \t" << BoldYellowFont << Material->StdDev_J[Layer] << " \t" << std::endl;
		std::cout << BoldCyanFont << "\tAnisotropy dispersion angle                = \t" << BoldYellowFont << Material->Anis_angle[Layer] << " degrees\t" << std::endl;
		if(Num_Layers>1){
			std::cout << BoldCyanFont << "\tH exchange out of plane_UP                 = \t" << BoldYellowFont << Material->Hexch_str_out_plane_UP[Layer] << " Oe\t" << std::endl;
			if(Layer!=0){
				std::cout << BoldCyanFont << "\tH exchange out of plane_DOWN               = \t" << BoldYellowFont << Material->Hexch_str_out_plane_DOWN[Layer] << " Oe\t" << std::endl;
		}	}
		std::cout << ResetFont << std::endl;
	}
	std::cout << "======================================================================\n" << std::endl;
	return 0;
}

