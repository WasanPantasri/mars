/*
 * Experiment_HAMR_long_timescale_import.cpp
 *
 *  Created on: 5 Dec 2018
 *      Author: Ewan Rannala
 */

#include <iostream>

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"

int HAMR_long_timescale_import(const ConfigFile cfg, const std::string Expt_type,
							   Expt_data_write_t*Expt_data_write,Expt_laser_t*Expt_laser,
							   Expt_H_app_t*Expt_Happ,Expt_timings_t*Expt_timings){

	std::cout << "Assigning experiment parameters...";
	std::vector<double> temporary;

	if(Expt_type=="Sigma_Tc"){
		std::cout << " Sigma Tc" << std::endl;
		//####################Retrieve values from hash table####################//
		Expt_timings->Initialisation_time = cfg.getValueOfKey<double>("Tc:Initialisation_time");
		Expt_timings->Application_time = cfg.getValueOfKey<double>("Tc:Application_time");
		Expt_timings->write_read_time = cfg.getValueOfKey<double>("Tc:Write_read_time");

		Expt_Happ->H_appl_MAG_max = cfg.getValueOfKey<double>("Tc:Field");
		temporary = cfg.getValueOfKey<std::vector<double>>("Tc:Field_unit");
		Expt_Happ->H_appl_unit.x = temporary[0];
		Expt_Happ->H_appl_unit.y = temporary[1];
		Expt_Happ->H_appl_unit.z = temporary[2];

		Expt_laser->Environ_temp = cfg.getValueOfKey<double>("Tc:Temp_background");
		Expt_laser->Laser_Temp_MIN = cfg.getValueOfKey<double>("Tc:Laser_Temp_min");
		Expt_laser->Laser_Temp_MAX = cfg.getValueOfKey<double>("Tc:Laser_Temp_max");
		Expt_laser->Laser_Temp_interval = cfg.getValueOfKey<double>("Tc:Laser_Temp_interval");
	}
	return 0;
}


