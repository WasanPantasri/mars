/* Experiment_import.cpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"


int HAMR_writing_expt_import(const ConfigFile cfg,const std::string Expt_Type,
							 Expt_data_write_t*Expt_data_write,Expt_laser_t*Expt_laser,
							 Expt_H_app_t*Expt_Happ,Expt_timings_t*Expt_timings){

	std::cout << "Assigning experiment parameters...";

	std::vector<double> temporary;

	// Determine desired experiment type.
	std::cout << " HAMR ";
	//####################Retrieve values from hash table####################//
	Expt_laser->Laser_Temp_MIN = cfg.getValueOfKey<double>("hamr:temp_min");
	Expt_laser->Laser_Temp_MAX = cfg.getValueOfKey<double>("hamr:temp_max");
	Expt_laser->cooling_time   = cfg.getValueOfKey<double>("hamr:cooling_time");

	Expt_Happ->H_appl_MAG_min = cfg.getValueOfKey<double>("hamr:Applied_field_minimum");
	Expt_Happ->H_appl_MAG_max = cfg.getValueOfKey<double>("hamr:applied_field_strength");
	Expt_Happ->Field_ramp_time = cfg.getValueOfKey<double>("hamr:Field_ramp_time");
	temporary = cfg.getValueOfKey<std::vector<double>>("hamr:applied_field_unit");
	Expt_Happ->H_appl_unit.x = temporary[0];
	Expt_Happ->H_appl_unit.y = temporary[1];
	Expt_Happ->H_appl_unit.z = temporary[2];

	Expt_timings->Meas_time = cfg.getValueOfKey<double>("hamr:Measurement_time");
	Expt_timings->Run_time = cfg.getValueOfKey<double>("hamr:run_time");

	if(Expt_Type=="HAMR_local_write" || Expt_Type=="HAMR_write_data"){
		std::cout << "writing ";
		//####################Retrieve values from hash table####################//
		Expt_Happ->Field_width_X = cfg.getValueOfKey<double>("hamr:Field_x_width");
		Expt_Happ->Field_width_Y = cfg.getValueOfKey<double>("hamr:Field_y_width");
		Expt_laser->Tprofile_width_X = cfg.getValueOfKey<double>("hamr:Beam_x_width");
		Expt_laser->Tprofile_width_Y = cfg.getValueOfKey<double>("hamr:Beam_y_width");

		if(Expt_Type=="HAMR_write_data"){
			std::cout << "data";
			Expt_data_write->Bit_width = cfg.getValueOfKey<double>("HAMR:Bit_width");
			Expt_data_write->Bit_length = cfg.getValueOfKey<double>("HAMR:Bit_length");
			Expt_data_write->Bit_spacing_X = cfg.getValueOfKey<double>("HAMR:Bit_spacing_X");
			Expt_data_write->Bit_spacing_Y = cfg.getValueOfKey<double>("HAMR:Bit_spacing_Y");
			Expt_data_write->Bit_number_X = cfg.getValueOfKey<int>("HAMR:Bit_number_in_x");
			Expt_data_write->Bit_number_Y = cfg.getValueOfKey<int>("HAMR:Bit_number_in_y");
			Expt_data_write->Data_Binary = cfg.getValueOfKey<std::string>("HAMR:Data_Binary");
			if(Expt_data_write->Data_Binary != "square-wave" && Expt_data_write->Data_Binary != "binary"){
				std::cout << " CFG error: Unknown Data write type. -> " << Expt_data_write->Data_Binary << std::endl;
				exit (EXIT_FAILURE);
			}
			else if(Expt_data_write->Data_Binary == "binary"){
				// Open and read in digits from file
				std::string File_location = "Input/" + cfg.getValueOfKey<std::string>("HAMR:Data_Location");
				std::ifstream BINARY_DATA(File_location.c_str());
				if(!BINARY_DATA){throw std::runtime_error("CFG error: " + File_location + " not found"  + "\n");}
				int value;
				//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
				for(int i=0;i<(Expt_data_write->Bit_number_X*Expt_data_write->Bit_number_Y);++i){
					value = BINARY_DATA.get();
					if(isdigit(value)){value = value - '0';}
					if(value==0){Expt_data_write->Writable_data.push_back(value-1);}
					else{Expt_data_write->Writable_data.push_back(value);}
			}	}
			else if(Expt_data_write->Data_Binary == "square-wave"){
				int value=1;
				Expt_data_write->Writable_data.resize((Expt_data_write->Bit_number_X*Expt_data_write->Bit_number_Y));
				for(int i=0;i<(Expt_data_write->Bit_number_X*Expt_data_write->Bit_number_Y);++i){
					Expt_data_write->Writable_data[i]=value;
					value *= -1;
		}	}	}
		std::cout << " simulation" << std::endl;
	}
	std::cout << std::endl;
	return 0;
}
