/* Grain_setup.cpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala
 */

//---------------------------------------------//
#ifdef USEDEBUG
#define Debug(x) std::cout << x
#else
#define Debug(x)
#endif
//---------------------------------------------//

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Distributions_generator.hpp"
#include "../../hdr/Globals.hpp"

#include <random>


int Grain_setup(const unsigned int Num_grains,const int Num_Layers,
		        const Material_t Material, const std::vector<double> Grain_area,
		        const double Temperature, Grain_t*Grain){

	std::cout << "Assigning grain data..." << std::endl;
	unsigned int Total_grains = Num_grains * Num_Layers;
	double phi, theta, EAx, EAy, EAz, EAx2, EAy2, EAz2;
	double Pi_o_180 = 0.0174532925199432957692369;

	// Resize all Vec3 vectors
	Grain->m.resize(Total_grains);
	Grain->Easy_axis.resize(Total_grains);
	Grain->H_appl.resize(Total_grains);
	// Ensure all other vectors are initially empty
	Grain->Temp.clear();
	Grain->Vol.clear();
	Grain->K.clear();
	Grain->Ms.clear();
	Grain->Tc.clear();

	/*
	 * NEED TO SET UP ANISOTROPY DISPERSION
	 * 		NEED TO ENSURE LIMIT IS [0:180) -> IF <0 COUNT BACK FROM 180 | IF >180 COUNT FORWARD FROM 180.
	 * 		GAUSSIAN DISTRIBUTION FOR POLAR ANGLE.
	 * 		NORMAL DISTRIBUTION FOR AZIMUTHAL ANGLE.
	 *
	 * 		1. GENERATE UNIFORM DISTRIBUTION IN AZIMUTHAL ANGLE
	 * 			    std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
	 * 			    phi = 2.0*PI*uniformPHI;
	 * 		2. GENERATE GAUSSIAN (UNIFORM?) DISTRIBUTION IN POLAR ANGLE [0:180)
	 * 				deg*pi/180 = rad
	 * 				Input 3 --> Input/180 should give correct uppper limit for distribution.
	 * 			    std::uniform_real_distribution<double> uniformTHETA(0.0, Input/180.0);
	 * 			    theta = acos(1.0-2.0*uniformTHETA);
	 * 		3. CONVERT POLAR COORDINATES TO CARTESIAN COORDINATES
	 * 			    x = sin(theta)*cos(phi);
	 * 			    y = sin(theta)*sin(phi);
	 * 			    x = cos(phi);
	 */


	for(int Layer=0;Layer<Num_Layers;++Layer){
		int Offset = Layer*Num_grains;
		// Polar and azimuthal angle generator -different per layer.
		std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
		std::uniform_real_distribution<double> uniformTHETA(0.0, Material.Anis_angle[Layer]/180.0);
		std::normal_distribution<double> Norm(0.0,1.0);
		for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
			unsigned int Grain_in_system = Grain_in_Layer+Offset;

			// Add random initial magnetisation
			if(Material.Mag_Type[Layer]=="assigned"){
				Grain->m[Grain_in_system].x=Material.Initial_mag[Layer].x;
				Grain->m[Grain_in_system].y=Material.Initial_mag[Layer].y;
				Grain->m[Grain_in_system].z=Material.Initial_mag[Layer].z;
			}
			else if(Material.Mag_Type[Layer]=="random"){
				double X=Norm(Gen),Y=Norm(Gen),Z=Norm(Gen);
				double MAG = X*X+Y*Y+Z*Z;
				// Normalise the magnetisation
				X /= MAG; Y /= MAG; Z /= MAG;
				Grain->m[Grain_in_system].x = X;
				Grain->m[Grain_in_system].y = Y;
				Grain->m[Grain_in_system].z = Z;
			}
			// Generate dispersion in anisotropy direction
			phi = 2.0*PI*uniformPHI(Gen);
			theta = acos(1.0-2.0*uniformTHETA(Gen));
			EAx=sin(theta)*cos(phi);
			EAy=sin(theta)*sin(phi);
			EAz=cos(theta);
			// Rotate easy axes to match desired initial direction
			EAx2 = EAx*cos(Material.Easy_axis_polar[Layer]*Pi_o_180) + EAz*sin(Material.Easy_axis_polar[Layer]*Pi_o_180);
			EAy2 = EAy;
			EAz2 = -EAx*sin(Material.Easy_axis_polar[Layer]*Pi_o_180) + EAz*cos(Material.Easy_axis_polar[Layer]*Pi_o_180);
			// Azimuthal rotation.
			Grain->Easy_axis[Grain_in_system].x = EAx2*cos(Material.Easy_axis_azimuth[Layer]*Pi_o_180) - EAy2*sin(Material.Easy_axis_azimuth[Layer]*Pi_o_180);
			Grain->Easy_axis[Grain_in_system].y = EAx2*sin(Material.Easy_axis_azimuth[Layer]*Pi_o_180) + EAy2*cos(Material.Easy_axis_azimuth[Layer]*Pi_o_180);
			Grain->Easy_axis[Grain_in_system].z = EAz2;
			//------------
			Grain->Temp.push_back(Temperature);
			Grain->Vol.push_back(Grain_area[Grain_in_Layer]*Material.dz[Layer]);
			Grain->Ms.push_back(Material.Ms[Layer]);
			Grain->Callen_power.push_back(Material.Callen_power[Layer]);
			Grain->Crit_exp.push_back(Material.Crit_exp[Layer]);

		}
		Dist_gen(Num_grains,Material.Tc_Dist_Type[Layer],Material.Avg_Tc[Layer],Material.StdDev_Tc[Layer],&Grain->Tc);
		Dist_gen(Num_grains,Material.K_Dist_Type[Layer],Material.Avg_K[Layer],Material.StdDev_K[Layer],&Grain->K);
	}
    std::cout << std::endl;
	return 0;
}


