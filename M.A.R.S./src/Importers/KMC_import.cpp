/*
 * KMC_import.cpp
 *
 *  Created on: 11 Dec 2018
 *      Author: Ewan Rannala
 */

#include <iostream>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int KMC_import(const ConfigFile cfg,KMC_t*KMC){

	KMC->time_step = cfg.getValueOfKey<double>("KMC:dt");
	KMC->ZeroKInputs = cfg.getValueOfKey<bool>("KMC:Zero_Kelvin_inputs");

	return 0;
}



