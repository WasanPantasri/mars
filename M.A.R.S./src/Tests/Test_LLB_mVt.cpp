/* Test_LLB_mVt.cpp
 *  Created on: 15 Aug 2018
 *      Author: Samuel Ewan Rannala
 */

#include <iostream>
#include <fstream>
#include "math.h"

#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLB.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

int LLB_mVt_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Expt_H_app_t Expt_field;
	Material_t Material;
	Interaction_t Int_Mat1;
	LLB_t LLB_data;
	Grain_t Grain;
	Vec3 Applied_Field;
	double Mx,My,Mz,Xpara,Xperp;
	std::ofstream LLB_Output("Tests/LLB_mVt/Output/LLB_mVt.dat");

//###########TEST PARAMETERS###########//
	double Mag_length_thresh=1e-5;
	double Temp_incr = 25.0;

	Structure.Dim_x = 50.0;
	Structure.Dim_y = 50.0;
	Structure.Num_layers = 1;
	Structure.Grain_width = 5.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Magneto_Interaction_Radius = 10.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Expt_field.H_appl_unit.x = 1.0;
	Expt_field.H_appl_unit.y = 0.0;
	Expt_field.H_appl_unit.z = 0.0;
	Expt_field.H_appl_MAG_max = 0.0;

	Material.Mag_Type.push_back("assigned");
	Material.Initial_mag.resize(1);
	Material.Initial_mag[0].x = 0.0;
	Material.Initial_mag[0].y = 0.0;
	Material.Initial_mag[0].z = 1.0;
	Material.Easy_axis_polar.push_back(0.0);
	Material.Easy_axis_azimuth.push_back(0.0);
	Material.Anis_angle.push_back(0.0);
	Material.z.push_back(0.0);
	Material.dz.push_back(10);
	Material.Ms.push_back(1800.8);
	Material.Tc_Dist_Type.push_back("normal");
	Material.Avg_Tc.push_back(700.123);
	Material.StdDev_Tc.push_back(0.0);
	Material.K_Dist_Type.push_back("normal");
	Material.Avg_K.push_back(2.0e+5);
	Material.StdDev_K.push_back(0.0);
	Material.Anis_angle.push_back(180.0);
	Material.J_Dist_Type.push_back("normal");
	Material.StdDev_J.push_back(0.2);
	Material.H_sat.push_back(300);
	Material.Callen_power.push_back(2);
	Material.Crit_exp.push_back(0.365);

	LLB_data.Alpha.resize(1);
	LLB_data.Alpha[0] = 0.1;
	LLB_data.dt = 1.0e-13;
	double Temperature = 0.0;
//#####################################//

	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area, Temperature, &Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Material,Voronoi_data,&Int_Mat1);

	Applied_Field.x = Expt_field.H_appl_unit.x*Expt_field.H_appl_MAG_max;
	Applied_Field.y = Expt_field.H_appl_unit.y*Expt_field.H_appl_MAG_max;
	Applied_Field.z = Expt_field.H_appl_unit.z*Expt_field.H_appl_MAG_max;

	for(int j=0;j<Structure.Num_layers;j++){
		int offset = j*Voronoi_data.Num_Grains;
		for(int i=0;i<Voronoi_data.Num_Grains;i++){
			Grain.H_appl[offset+i].x = Applied_Field.x;
			Grain.H_appl[offset+i].y = Applied_Field.y;
			Grain.H_appl[offset+i].z = Applied_Field.z;
	}	}

	std::cout << "Performing M vs. T test..." << std::endl;
	double Mag_length, Mag_length_AVG, Mag_length_1, Mag_length_1AVG, Mag_length_2, Mag_length_2AVG;
	int Rep_counter=0;
	for(int Temp=0;Temp<801;Temp+=Temp_incr){
		Rep_counter=0.0;
		Mx=My=Mz=Xpara=Xperp=Mag_length_AVG=0.0;
		std::cout << Temp << "K\n";
		for(int grain=0;grain<Voronoi_data.Num_Grains;grain++){
			Grain.Temp[grain] = Temp;
			Mag_length_1 = sqrt(Grain.m[grain].x*Grain.m[grain].x+Grain.m[grain].y*Grain.m[grain].y+Grain.m[grain].z*Grain.m[grain].z);
			Mag_length_1AVG += Mag_length_1;
		}
		Mag_length_1AVG /= Voronoi_data.Num_Grains;
		Mag_length_2AVG = Mag_length_1AVG;
		do{
			Mag_length_1AVG = Mag_length_2AVG;
			LL_B(Voronoi_data.Num_Grains,1.0,Int_Mat1,Voronoi_data,&LLB_data,&Grain);
			for(int grain=0;grain<Voronoi_data.Num_Grains;grain++){
				Mag_length_2 = sqrt(Grain.m[grain].x*Grain.m[grain].x+Grain.m[grain].y*Grain.m[grain].y+Grain.m[grain].z*Grain.m[grain].z);
				Mag_length_2AVG += Mag_length_2;
			}
			Mag_length_2AVG /=Voronoi_data.Num_Grains;
			++Rep_counter;
			std::cout << Rep_counter <<  " | " << fabs(Mag_length_1AVG-Mag_length_2AVG) << "\r";
			if(Rep_counter>15000000){std::cout << "Too many reps" << std::endl; return 1;}
		}
		while (fabs(Mag_length_1AVG-Mag_length_2AVG)>Mag_length_thresh || Rep_counter < 10000);
		std::cout << "\n";

		for(int grain=0;grain<Voronoi_data.Num_Grains;grain++){
			Mag_length = sqrt(Grain.m[grain].x*Grain.m[grain].x+Grain.m[grain].y*Grain.m[grain].y+Grain.m[grain].z*Grain.m[grain].z);
			Mag_length_AVG += Mag_length;
			Mx += Grain.m[grain].x;
			My += Grain.m[grain].y;
			Mz += Grain.m[grain].z;
			Xpara += LLB_data.Chi_para[grain];
			Xperp += LLB_data.Chi_perp[grain];
		}
		Mx /= Voronoi_data.Num_Grains; My /= Voronoi_data.Num_Grains;	Mz /= Voronoi_data.Num_Grains;
		Mag_length_AVG/= Voronoi_data.Num_Grains; Xpara /= Voronoi_data.Num_Grains; Xperp /= Voronoi_data.Num_Grains;
		LLB_Output << Mx << " " << My << " " << Mz << " " << Mag_length_AVG << " " << Grain.Temp[0] << " " << Grain.Tc[0]
		           << " " << Xpara <<  " " << Xperp << " " << LLB_data.m_EQ[0] << std::endl;
	}
	return 0;
}


