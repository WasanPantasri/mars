/*
 * Test_KMC_SW.cpp
 *
 *  Created on: 19 Nov 2018
 *      Author: Ewan Rannala
 */


#include <iostream>
#include <fstream>
#include "math.h"

#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Tests/Test_LLG_hyst.hpp" // Provides the DotP function
#include "../../hdr/Solvers/KMC_Solver.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

int KMC_SW_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Materials;
	Interaction_t Int_system;
	KMC_t KMC_data;
	Grain_t Grain;
	int H_step=600;
	double Phi_H=0.0, time=0.0, m_thresh=1e-20;

	std::cout << "Performing SW hysteresis test for KMC..." << std::endl;

//###########TEST PARAMETERS###########//

	Structure.Dim_x = 9.0;
	Structure.Dim_y = 5.0;
	Structure.Num_layers= 1;
	Structure.Grain_width = 5.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Materials.Mag_Type.push_back("assigned");
	Materials.Initial_mag.resize(Structure.Num_layers);
	Materials.z.push_back(0.0);
	for(int i=0;i<Structure.Num_layers;++i){
		Materials.Initial_mag[i].x=0.0;
		Materials.Initial_mag[i].y=0.0;
		Materials.Initial_mag[i].z=1.0;

		Materials.Easy_axis_polar.push_back(0.0);
		Materials.Easy_axis_azimuth.push_back(0.0);
		Materials.Anis_angle.push_back(0.0);
 		if(i>0){Materials.z.push_back(Materials.z[i-1]+(Materials.dz[i-1]+Materials.dz[i])*0.5);}
		Materials.dz.push_back(10);
		Materials.Ms.push_back(1.0);
		Materials.Tc_Dist_Type.push_back("normal");
		Materials.Avg_Tc.push_back(700);
		Materials.StdDev_Tc.push_back(0.0);
		Materials.K_Dist_Type.push_back("normal");
		Materials.Avg_K.push_back(1.0e+5);
		Materials.StdDev_K.push_back(0.0);
		Materials.J_Dist_Type.push_back("normal");
		Materials.StdDev_J.push_back(0.0);
		Materials.H_sat.push_back(0);
		Materials.Callen_power.push_back(0.0);
		Materials.Crit_exp.push_back(0.0);
	}
	KMC_data.time_step = 1.0; // 1 second
	KMC_data.ZeroKInputs = false;

	double Temp=1e-9;

//#####################################//

	Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Materials,Voronoi_data,&Int_system);

	std::vector<std::ofstream> KMC_Output (Voronoi_data.Num_Grains);
	std::vector<double> H_k_MAG, H_appl_MAG, H_delta;

	for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		Vec3 H_appl_unit, Magnetisation;

		std::string FileLOC = "Tests/KMC_SW/Output/KMC_SW_" + std::to_string(grain) + ".dat";
		KMC_Output[grain].open(FileLOC.c_str());
		KMC_Output[grain] << "mx my mz Hx Hy Hz Ex Ey Ez H_MAG Hk m.H time\n";

		H_k_MAG.push_back((2.0*Grain.K[grain])/Grain.Ms[grain]);
		H_appl_MAG.push_back(1.5*H_k_MAG[grain]);
		H_delta.push_back((4.0*H_appl_MAG[grain])/H_step);

		std::cout << "Grain " << grain << " of " << Voronoi_data.Num_Grains << std::endl;
		for(double Theta_H = 90.0; Theta_H>-1.0; Theta_H-=15.0){
			std::cout << "Simulating at " << Theta_H << " Degrees" << std::endl;

			H_appl_MAG[grain] = 1.5*H_k_MAG[grain];

			if(Theta_H == 0.0){Theta_H=0.1;}
			else if(Theta_H == 90.0){Theta_H=89.9;}

			int step_count=0, rep_count=0;
			H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
			H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
			H_appl_unit.z = cos(Theta_H*PI/180.0);
			Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG[grain];
			Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG[grain];
			Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG[grain];
			Magnetisation.x = Grain.m[grain].x;
			Magnetisation.y = Grain.m[grain].y;
			Magnetisation.z = Grain.m[grain].z;

			std::cout <<  "\tStep: " << "0" << " of " << H_step << "\r" << std::flush;
			while(step_count<=H_step){
				KMC_solver(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,KMC_data.time_step,KMC_data.ZeroKInputs,false,1.0e9,&Grain);
				time += KMC_data.time_step;

				if(fabs(Grain.m[grain].x-Magnetisation.x) < m_thresh &&
				   fabs(Grain.m[grain].y-Magnetisation.y) < m_thresh &&
				   fabs(Grain.m[grain].z-Magnetisation.z) < m_thresh){
					// OUTPUT TO FILE
					KMC_Output[grain] << Grain.m[grain].x << " " << Grain.m[grain].y << " " << Grain.m[grain].z << " "
							   	  << H_appl_unit.x << " " << H_appl_unit.y << " " << H_appl_unit.z << " "
							   	  << Grain.Easy_axis[grain].x << " " << Grain.Easy_axis[grain].y << " " << Grain.Easy_axis[grain].z << " "
							   	  << H_appl_MAG[grain] << " " << H_k_MAG[grain] << " " << DotP(Grain.m[grain],H_appl_unit) << " " << time << "\n" << std::flush;

					if(step_count < H_step/2.0){H_appl_MAG[grain] -= H_delta[grain];}
					else{H_appl_MAG[grain] += H_delta[grain];}
					++step_count;
					rep_count=0;
					Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG[grain];
					Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG[grain];
					Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG[grain];

					Magnetisation.x = Grain.m[grain].x;
					Magnetisation.y = Grain.m[grain].y;
					Magnetisation.z = Grain.m[grain].z;
					std::cout <<  "\tStep: " << "0" << " of " << H_step << "\r" << std::flush;
				}
				else{
					for(int i=0;i<Voronoi_data.Num_Grains;++i){
						Magnetisation.x = Grain.m[grain].x;
						Magnetisation.y = Grain.m[grain].y;
						Magnetisation.z = Grain.m[grain].z;
					}
					std::cout << "\t\tRepetition: " << rep_count << "\r" << std::flush;
					++rep_count;
					if(rep_count >= 15000000){
						std::cout << "Too many steps" << std::endl;
						KMC_Output[grain].close();
						return 1;
			}	}	}
			if(Theta_H == 0.1){Theta_H=0.0;}
			else if(Theta_H == 89.9){Theta_H=90.0;}
			KMC_Output[grain] << "\n\n";
			time=0.0;
			Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Temp,&Grain);
		}
	}
	return 0;
}
