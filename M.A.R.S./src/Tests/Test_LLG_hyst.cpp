/* Test_LLG_hyst.cpp
 *  Created on: 23 May 2018
 *      Author: Samuel Ewan Rannala
 */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"

// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLG.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

double DotP(const Vec3 VectorA, const Vec3 VectorB){
	double DOT;
	DOT = VectorA.x*VectorB.x + VectorA.y*VectorB.y + VectorA.z*VectorB.z;
	return DOT;
}


int LLG_hyst_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Material;
	Interaction_t Int_Mat1;
	LLG_t LLG_data;
	Grain_t GrainA;
	Vec3 H_appl_unit;
	int H_step=600;
	double Phi_H=0.0, time=0.0, m_thresh=1e-20, H_delta;
	std::vector<Vec3> Magnetisation;

	std::cout << "Performing LLG hysteresis test... \n" << std::flush;

	std::ofstream LLG_OUTPUT("Tests/LLG_hysteresis/Output/LLG.dat");
	LLG_OUTPUT << "mx my mz Hx Hy Hz Ex Ey Ez H_MAG Hk m.H time\n";

// Generate system parameters
	Structure.Dim_x = 9.0;
	Structure.Dim_y = 5.0;
	Structure.Grain_width = 5.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Num_layers= 1;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Material.Mag_Type.push_back("assigned");
	Material.Initial_mag.resize(1);
	Material.Initial_mag[0].x=0.0;
	Material.Initial_mag[0].y=0.0;
	Material.Initial_mag[0].z=1.0;
	Material.Easy_axis_polar.push_back(0.0);
	Material.Easy_axis_azimuth.push_back(0.0);
	Material.Anis_angle.push_back(0.0);
	Material.z.push_back(0.0);
	Material.dz.push_back(10);
	Material.Ms.push_back(1.0);
	Material.K_Dist_Type.push_back("normal");
	Material.Avg_K.push_back(1.0);
	Material.StdDev_K.push_back(0.0);
	Material.Tc_Dist_Type.push_back("normal");
	Material.Avg_Tc.push_back(700);
	Material.StdDev_Tc.push_back(0.0);
	Material.J_Dist_Type.push_back("normal");
	Material.StdDev_J.push_back(0.0);
	Material.H_sat.push_back(0);
	Material.Callen_power.push_back(0.0);
	Material.Crit_exp.push_back(0.0);

	LLG_data.Alpha.resize(1);
	LLG_data.Alpha[0] = 0.1;
	LLG_data.dt=1.0e-10;
	LLG_data.Gamma = 1.7e+7;
	double Temp=0;

	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Temp,&GrainA);
	Generate_interactions(Structure.Num_layers,GrainA.Vol,Structure.Magneto_Interaction_Radius,Material,Voronoi_data,&Int_Mat1);

	Magnetisation.resize(Voronoi_data.Num_Grains);
	double H_k_MAG = (2.0*GrainA.K[0])/GrainA.Ms[0];
	double H_appl_MAG = 1.5*H_k_MAG;
	H_delta = (4.0*H_appl_MAG)/H_step;

	for(double Theta_H = 90.0; Theta_H>-1.0; Theta_H-=15.0){
		H_appl_MAG = 1.5*H_k_MAG;
		std::cout << "Simulating at " << Theta_H << " Degrees" << std::endl;

		if(Theta_H == 0.0){Theta_H=0.1;}
		else if(Theta_H == 90.0){Theta_H=89.9;}

		int step_count=0, rep_count=0;
		H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
		H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
		H_appl_unit.z = cos(Theta_H*PI/180.0);
		for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){
			GrainA.H_appl[grain].x=H_appl_unit.x*H_appl_MAG;
			GrainA.H_appl[grain].y=H_appl_unit.y*H_appl_MAG;
			GrainA.H_appl[grain].z=H_appl_unit.z*H_appl_MAG;
		}
		for(int i=0;i<Voronoi_data.Num_Grains;++i){
			Magnetisation[i].x = GrainA.m[i].x;
			Magnetisation[i].y = GrainA.m[i].y;
			Magnetisation[i].z = GrainA.m[i].z;
		}
		std::cout << "\tStep: " << "0" << " of " << H_step << "\r" << std::flush;
		while(step_count<=H_step){
			LL_G(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat1,Voronoi_data,LLG_data,false,&GrainA);
			time += LLG_data.dt;

			if(fabs(GrainA.m[0].x-Magnetisation[0].x) < m_thresh &&
			   fabs(GrainA.m[0].y-Magnetisation[0].y) < m_thresh &&
			   fabs(GrainA.m[0].z-Magnetisation[0].z) < m_thresh){
				// OUTPUT TO FILE
				LLG_OUTPUT << GrainA.m[0].x << " " << GrainA.m[0].y << " " << GrainA.m[0].z << " "
						   << H_appl_unit.x << " " << H_appl_unit.y << " " << H_appl_unit.z << " "
						   << GrainA.Easy_axis[0].x << " "
						   << GrainA.Easy_axis[0].y << " " << GrainA.Easy_axis[0].z << " "
						   << H_appl_MAG << " " << H_k_MAG << " " << DotP(GrainA.m[0],H_appl_unit) << " " << time << "\n" << std::flush;
				if(step_count < H_step/2.0){H_appl_MAG -= H_delta;}
				else{H_appl_MAG += H_delta;}
				++step_count;
				rep_count=0;
				for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					GrainA.H_appl[grain].x=H_appl_unit.x*H_appl_MAG;
					GrainA.H_appl[grain].y=H_appl_unit.y*H_appl_MAG;
					GrainA.H_appl[grain].z=H_appl_unit.z*H_appl_MAG;
				}
				for(int i=0;i<Voronoi_data.Num_Grains;++i){
					Magnetisation[i].x = GrainA.m[i].x;
					Magnetisation[i].y = GrainA.m[i].y;
					Magnetisation[i].z = GrainA.m[i].z;
				}
				std::cout << "\tStep: " << step_count << " of " << H_step << "\r" << std::flush;
			}
			else{
				for(int i=0;i<Voronoi_data.Num_Grains;++i){
					Magnetisation[i].x = GrainA.m[i].x;
					Magnetisation[i].y = GrainA.m[i].y;
					Magnetisation[i].z = GrainA.m[i].z;
				}
				std::cout << "\t\tRepetition: " << rep_count << "\r" << std::flush;
				++rep_count;
				if(rep_count >= 15000000){
					std::cout << "Too many steps" << std::endl;
					LLG_OUTPUT.close();
					return 1;
				}
			}
		}
		if(Theta_H == 0.1){Theta_H=0.0;}
		else if(Theta_H == 89.9){Theta_H=90.0;}

		LLG_OUTPUT << "\n\n";
		Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Temp,&GrainA);
	}
	return 0;
}


