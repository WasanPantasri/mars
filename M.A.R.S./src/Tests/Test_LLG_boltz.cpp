/* Test_LLG_boltz.cpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"

// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLG.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"


int LLG_boltz_test(){
	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat1;
	LLG_t LLG_data;
	Grain_t Grain;
	std::vector<double> m_theta, mx, my, mz;
	double time=0.0;
	std::ofstream LLG_OUTPUT("Tests/LLG_boltzmann/Output/LLG.dat");

	std::cout << "Performing LLG Boltzmann test... \n" << std::flush;

	LLG_OUTPUT << "theta_m, frequency bin_width total_values\n";

// Generate system parameters
	Structure.Dim_x = 20.0;
	Structure.Dim_y = 20.0;
	Structure.Grain_width = 8.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Num_layers = 1.0;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Material.Mag_Type.push_back("assigned");
	Material.Initial_mag.resize(1);
	Material.Initial_mag[0].x=0.0;
	Material.Initial_mag[0].y=0.0;
	Material.Initial_mag[0].z=1.0;
	Material.Easy_axis_polar.push_back(0.0);
	Material.Easy_axis_azimuth.push_back(0.0);
	Material.Anis_angle.push_back(0.0);
	Material.z.push_back(0.0);
	Material.dz.push_back(10);
	Material.Ms.push_back(1200);
	Material.K_Dist_Type.push_back("normal");
	Material.Avg_K.push_back(4.2e+6);
	Material.StdDev_K.push_back(0.0);
	Material.Tc_Dist_Type.push_back("normal");
	Material.Avg_Tc.push_back(700);
	Material.StdDev_Tc.push_back(0.0);
	Material.J_Dist_Type.push_back("normal");
	Material.StdDev_J.push_back(0.0);
	Material.H_sat.push_back(0);
	Material.Callen_power.push_back(0.0);
	Material.Crit_exp.push_back(0.0);

	LLG_data.Alpha.resize(1);
	LLG_data.Alpha[0] = 0.1;
	LLG_data.dt=1.0e-13;
	LLG_data.Gamma=1.7e+7;
	double Temp=1200;

	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Material,Voronoi_data,&Int_Mat1);

	std::cout << "Vol: " << Grain.Vol[0] << "nm^3" << std::endl;

	// SETUP APPLIED FIELD
	for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		Grain.H_appl[grain].x=0.0;
		Grain.H_appl[grain].y=0.0;
		Grain.H_appl[grain].z=0.0;
	}
	// RUN LLG
	int steps=0;
	while(time<1e-6){
		std::cout << "Time: " << time << "\r";
		LL_G(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat1,Voronoi_data,LLG_data,false,&Grain);
		double Mag = sqrt(Grain.m[0].x*Grain.m[0].x + Grain.m[0].y*Grain.m[0].y + Grain.m[0].z*Grain.m[0].z);
		m_theta.push_back(acos(Grain.m[0].z/Mag));
		time += LLG_data.dt;
		steps++;
	}
	// BIN MAGNETISATION
	double L = 2.0; //May need changing for large stability ratios
	int N = 2000;
	double dL = L/N;
	long int Bin[N];
	for(int i=0;i<N;i++){
		Bin[i] = 0;
	}
	int Bin_pos=0, i=0;
	while(i<steps){
		Bin_pos = (m_theta[i]/dL);
		Bin[Bin_pos]++;
		i++;
	}
	for(int count=0;count<N;count++){
		LLG_OUTPUT << count*dL << " " << Bin[count] << " " << dL << " " << steps*dL << "\n";
	}
	LLG_OUTPUT.flush();
	LLG_OUTPUT.close();
	return 0;
}


