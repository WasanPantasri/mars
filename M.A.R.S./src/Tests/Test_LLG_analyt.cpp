/* LLG_analyt_test.cpp
 *  Created on: 22 May 2018
 *      Author: Samuel Ewan Rannala
 */

// System header files
#include <iostream>
#include <fstream>

//M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Solvers/LLG.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"


int LLG_analyt_test(){

	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat1;
	LLG_t LLG_data;
	Grain_t Grain;
	int steps;
	std::ofstream LLG_OUTPUT("Tests/LLG_analytical/Output/LLG.dat");
	std::cout << "Performing LLG analytical test... \n" << std::flush;

// Generate system parameters
	Structure.Dim_x = 20.0;
	Structure.Dim_y = 20.0;
	Structure.Grain_width = 5.0;
	Structure.StdDev_grain_pos = 0.0;
	Structure.Packing_fraction = 1.0;
	Structure.Num_layers = 1;
	Structure.Magneto_Interaction_Radius = 0.0;
	Structure.Magnetostatics_gen_type = "dipole";

	Material.Mag_Type.push_back("assigned");
	Material.Initial_mag.resize(1);
	Material.Initial_mag[0].x=1.0;
	Material.Initial_mag[0].y=0.0;
	Material.Initial_mag[0].z=0.0;
	Material.Easy_axis_polar.push_back(0.0);
	Material.Easy_axis_azimuth.push_back(0.0);
	Material.Anis_angle.push_back(0.0);
	Material.z.push_back(0.0);
	Material.dz.push_back(10);
	Material.Ms.push_back(1.0);
	Material.K_Dist_Type.push_back("normal");
	Material.Avg_K.push_back(2.0e+5);
	Material.StdDev_K.push_back(0.0);
	Material.Tc_Dist_Type.push_back("normal");
	Material.Avg_Tc.push_back(700);
	Material.StdDev_Tc.push_back(0.0);
	Material.J_Dist_Type.push_back("normal");
	Material.StdDev_J.push_back(0.0);
	Material.H_sat.push_back(0);
	Material.Callen_power.push_back(0.0);
	Material.Crit_exp.push_back(0.0);

	LLG_data.Alpha.resize(1);
	LLG_data.Alpha[0] = 0.1;
	LLG_data.dt=1.0e-12;
	double Temp = 0;

	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Temp,&Grain);
	// Set easy axis to zero. Set applied field.
	for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		Grain.Easy_axis[grain].x=0.0;
		Grain.Easy_axis[grain].y=0.0;
		Grain.Easy_axis[grain].z=0.0;
		Grain.H_appl[grain].x=0.0;
		Grain.H_appl[grain].y=0.0;
		Grain.H_appl[grain].z=400.0*PI;

	}
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Material,Voronoi_data,&Int_Mat1);

	steps = int(2e-9/LLG_data.dt);

	std::cout << "Performing LLG steps..." << std::endl;
	for(int step=0; step<steps; ++step){
		std::cout << "Step: " << step+1 << " out of " << steps << "\r";
		LL_G(Voronoi_data.Num_Grains,Structure.Num_layers,Int_Mat1,Voronoi_data,LLG_data,false,&Grain);
		LLG_OUTPUT << Grain.m[0].x << " " << Grain.m[0].y << " " << Grain.m[0].z << " " << step*LLG_data.dt << "\n";
	}
	LLG_OUTPUT.flush();LLG_OUTPUT.close();
	return 0;
}
