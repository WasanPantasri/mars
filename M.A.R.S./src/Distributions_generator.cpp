/* Distributions_generator.cpp
 *  Created on: 31 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <random>
#include <vector>
#include <iostream>
#include "math.h"

#include "../hdr/Globals.hpp"

int Dist_gen(const unsigned int Num_grains,const std::string Type, const double Mean, const double StdDev, std::vector<double>*Data){
//	std::mt19937 Gen(12345);
	double Val;
	if(Type == "normal"){
		std::normal_distribution<double> Norm(Mean,StdDev);
		for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
			Val = Norm(Gen);
			Data->push_back(Val);
	}	}
	else{
		// Need to adjust the Mean and Standard deviation for use in log-normal distribution.
		double LogMean, LogStdDev;
		LogMean   = log( Mean/sqrt(1.0+((StdDev*StdDev)/(Mean*Mean))));
		LogStdDev = sqrt(log(1.0+(StdDev*StdDev)/(Mean*Mean)));

		std::lognormal_distribution<double> LogNorm(LogMean,LogStdDev);
		for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
			Val = LogNorm(Gen);
			Data->push_back(Val);
	}	}
	return 0;
}



