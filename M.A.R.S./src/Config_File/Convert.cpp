/* Convert.cpp
 *  Created on: 28 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include "../../hdr/Config_File/Convert.hpp"
#include <sstream>
#include <typeinfo>

const std::string Convert::BoldRedFont = "\033[1;4;31m";
const std::string Convert::ResetFont = "\033[0m";

template <typename T>
std::string Convert::T_to_string(const std::string FileName, T const &val, std::string const &key){
    // Convert T, which should be a primitive, to a std::string.
	std::ostringstream outstring;
	outstring << val;
	return outstring.str();
}

template <typename T>
T Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
    // Convert a std::string to T.
	std::istringstream istr(val);
	T returnVal;
	if (!(istr >> returnVal)){
		throw std::runtime_error(BoldRedFont + "CFG error: Invalid " + (std::string)typeid(T).name() + " received from " + key + " in " + FileName + ResetFont + "\n");
	}
	return returnVal;
}

template <>
std::string Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	return val;
}

template <>
bool Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	bool returnVal;
	if(val=="true"){returnVal=true;}
	else if(val=="false"){returnVal=false;}
	else{
		throw std::runtime_error(BoldRedFont + "CFG error: Invalid Boolean received from " + key + " in " + FileName + ResetFont + "\n");
	}
	return returnVal;
}

template <>
std::vector<double> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	double number;
	std::vector<double> Numbers;
	while (istr >> number || !istr.eof()){
		if (istr.fail()){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid double received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Numbers.push_back(number);
	}
	return Numbers;
}

template <>
std::vector<int> Convert::string_to_T(const std::string FileName, std::string const &val, std::string const &key){
	std::string temp=val;
	std::stringstream istr(temp);
	int number;
	std::vector<int> Numbers;
	while (istr >> number || !istr.eof()){
		if (istr.fail()){
			throw std::runtime_error(BoldRedFont + "CFG error: Invalid int received from " + key + " in " + FileName + ResetFont + "\n");
		}
		Numbers.push_back(number);
	}
	return Numbers;
}

template std::string Convert::T_to_string(const std::string, std::string const&, std::string const &);
template std::string Convert::T_to_string(const std::string, double const&, std::string const &);

template double Convert::string_to_T(const std::string, std::string const&, std::string const &);
template int Convert::string_to_T(const std::string, std::string const&, std::string const &);
template bool Convert::string_to_T(const std::string, std::string const&, std::string const &);



