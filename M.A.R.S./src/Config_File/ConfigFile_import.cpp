/* ConfigFile_import.cpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#include <algorithm>
#include <fstream>

#include "../../hdr/Config_File/ConfigFile_import.hpp"

void ConfigFile::removeComment(std::string &line) const{
	// If ; is present remove ; and all characters following.
	if(line.find('#') != line.npos){
		line.erase(line.find('#'));
	}
	if(line.find(';') != line.npos){
		line.erase(line.find(';'));
	}
}

void ConfigFile::lowerCase(std::string &line) const{
	// Remove all upper case characters.
	transform(line.begin(),line.end(),line.begin(),::tolower);
}

bool ConfigFile::onlyWhitespace(const std::string &line) const{
	// True if only whitespace is present.
	return (line.find_first_not_of(' ') == line.npos);
}

void ConfigFile::FormatVector(std::string &line, size_t const lineNum) const{
	// Remove braces and replace commas with whitespace.
	line.erase(line.find('{'),1);
	if(line.find('}') == line.npos){
		throw std::runtime_error(BoldRedFont + "CFG error: Bad vector input, missing '}' on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	line.erase(line.find('}'),1);
	std::replace(line.begin(),line.end(), ',' , ' ');
}

bool ConfigFile::validLine(const std::string &line) const{
	std::string temp = line;
	temp.erase(0, temp.find_first_not_of('\t'));
	if(temp[0]=='='){return false;} // Invalid line if no key is present.
	for(size_t i=temp.find('=')+1;i<temp.length();++i){
		if(temp[i] != ' ' && temp[i] != '\t'){return true;} // Valid line of key and value are found.
	}
	return false;
}

void ConfigFile::extractKey(std::string &key, size_t const &separator_pos, const std::string &line) const{
	// Set key as substring before '=' and remove any white space.
	key=line.substr(0,separator_pos);
	if(key.find("/t")!=line.npos || key.find(" ")!=line.npos){
		key.erase(key.find_first_of("\t "));
	}
}

void ConfigFile::extractValue(std::string &value, size_t const &separator_pos, const std::string &line) const{
	// Set value as substring after '=' and remove any whitespace.
	value = line.substr(separator_pos+1);
	value.erase(0,value.find_first_not_of("\t "));
	value.erase(value.find_last_not_of("\t ")+1);
}

void ConfigFile::extractContents(const std::string &line){
	std::string temp=line;
	temp.erase(0, temp.find_first_not_of("\t "));
	size_t separator_pos = temp.find("=");
	std::string key, value;
	extractKey(key, separator_pos, temp);
	extractValue(value, separator_pos, temp);
	if(!keyExists(key)){
		contents.insert(std::pair<std::string, std::string>(key, value));
	}
	else{throw std::runtime_error(BoldRedFont + "CFG error: Key name conflict, all keys must be unique. -> " + key + " in " + FileName + ResetFont + "\n");}
}

void ConfigFile::parseLine(const std::string &line, size_t const lineNum){
	if (line.find("=") == line.npos){
		throw std::runtime_error(BoldRedFont + "CFG error: Couldn't find separator on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	if (!validLine(line)){
		throw std::runtime_error(BoldRedFont + "CFG error: Bad format on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	extractContents(line);
}

void ConfigFile::ExtractKeys(){
	std::ifstream File(FileName.c_str());
	if(!File){throw std::runtime_error(BoldRedFont + "CFG error: " + FileName + " not found"  + ResetFont + "\n");}
	std::string line;
	size_t lineNum=0;
	while(std::getline(File,line)){
		lineNum++;
		std::string temp=line;
		if(temp.empty()){continue;}
		removeComment(temp);
		if(onlyWhitespace(temp)){continue;}
		if(temp.find("{")!=temp.npos){FormatVector(temp,lineNum);}
		lowerCase(temp);
		parseLine(temp,lineNum);
		}
	File.close();
}

ConfigFile::ConfigFile(const std::string &FileName){
	this->FileName = FileName;
	ExtractKeys();
}

bool ConfigFile::keyExists(const std::string &key) const{
	std::string temp=key;
	lowerCase(temp);
	return contents.find(temp) != contents.end();
}

template <typename ValueType>
ValueType ConfigFile::getValueOfKey(const std::string &key) const{
	std::string temp=key;
	lowerCase(temp);
	if (!keyExists(temp)){
		throw std::runtime_error(BoldRedFont + "CFG error: Key not found -> " + temp + " in " + FileName + ResetFont + "\n");
	}
	return Convert::string_to_T<ValueType>(FileName, contents.find(temp)->second, temp);
}

template std::string ConfigFile::getValueOfKey<std::string>(const std::string&) const;
template std::vector<double> ConfigFile::getValueOfKey<std::vector<double>>(const std::string&) const;
template std::vector<int> ConfigFile::getValueOfKey<std::vector<int>>(const std::string&) const;
template double ConfigFile::getValueOfKey<double>(const std::string&) const;
template int ConfigFile::getValueOfKey<int>(const std::string&) const;
template bool ConfigFile::getValueOfKey<bool>(const std::string&) const;

