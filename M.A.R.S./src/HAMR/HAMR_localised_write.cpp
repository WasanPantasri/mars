/*
 * HAMR_localised_write.cpp
 *
 *  Created on: 2 Oct 2018
 *      Author: ewan
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/LLB_import.hpp"
#include "../../hdr/Importers/Experiments/Experiment_HAMR_writing_import.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Solvers/LLB.hpp"
#include "../../hdr/HAMR/Temperature_profile.hpp"
#include "../../hdr/HAMR/Field_profile.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"


#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"

int HAMR_localised_write(ConfigFile cfg){

	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Expt_timings_t Expt_timings;
	Expt_H_app_t Expt_field;
	Expt_data_write_t Expt_data;
	Expt_laser_t Expt_laser;
	Material_t Materials;
	Interaction_t Int_system;
	LLB_t LLB_data;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml;
	int Tot_grains,output_steps,file_writes,File_count=0,Sim_time_predictor=0;
	double temperature, MxT,MyT,MzT,MlT,Temp_norm, Field_MAG, Time=0.0,
			Head_X, Head_Y, Profile_tot_time;
	std::string FILENAME_idv;
	std::string Output_switch_file_loc = "Output/HAMR_Grain_switching.dat";
	std::vector<std::string> Output_switch_buffer;

//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	HAMR_writing_expt_import(cfg, "HAMR_local_write",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	LLB_import(cfg, Materials_Config, Structure.Num_layers, &LLB_data);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area, Expt_laser.Laser_Temp_MIN, &Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Materials,Voronoi_data,&Int_system);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###################DETERMINE IMPORTANT VALUES###################//

	Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;
	output_steps = round(Expt_timings.Meas_time/LLB_data.dt);
	temperature = Expt_laser.Laser_Temp_MIN;
	Field_MAG = Expt_field.H_appl_MAG_min;
// Want these to be assigned from input.
	Head_X=Voronoi_data.Centre_X;
	Head_Y=Voronoi_data.Centre_Y;

	Profile_tot_time = 6.0*Expt_laser.cooling_time;
	if(Expt_timings.Run_time > Profile_tot_time){file_writes = int(Expt_timings.Run_time/Expt_timings.Meas_time);}
	else{file_writes = int(Profile_tot_time/Expt_timings.Meas_time);}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_switch_buffer);

	std::cout << "Output steps: " << output_steps << std::endl;
	auto start = std::chrono::system_clock::now();

	while(Time<Expt_timings.Run_time){

		while(Time<Profile_tot_time){
			for(int Steps=0;Steps<output_steps;Steps++){
				Temperature_profile_spatial(Structure.Num_layers,Voronoi_data,Expt_laser,Head_X,Head_Y,Time,&Grain);
				Field_profile_spatial(Structure.Num_layers,Voronoi_data,Expt_field,Expt_laser.cooling_time,Head_X,Head_Y,Time,LLB_data.dt,&Field_MAG,&Grain);
				LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,Voronoi_data,&LLB_data,&Grain);
				Time += LLB_data.dt;
			}

// Could do with having layer specific file outputs?
			FILENAME_idv = std::to_string(File_count) + ".dat";
			Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
			++File_count;

			// Predict simulation run time
			++Sim_time_predictor;
			if(Sim_time_predictor==10){
				auto end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
				double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
				std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
			}
		}
		// Continue simulation but no longer apply T or H profiles.
		for(int Steps=0;Steps<output_steps;Steps++){
			LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,Voronoi_data,&LLB_data,&Grain);
			Time += LLB_data.dt;
		}
		FILENAME_idv = std::to_string(File_count) + ".dat";
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
		++File_count;
	}
	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_switch_buffer,Output_switch_file_loc);
	return 0;
}



