/*
 * Temperature_profile.cpp
 *
 *  Created on: 5 Sep 2018
 *      Author: ewan
 */

#include "math.h"

#include "../../hdr/Structures.hpp"

/* Calculates the temperature at the specified TIME for a Gaussian temperature profile.
 * The profile is such that the peak temperature occurs at 3*cooling_time. */
int Temperature_profile(const Expt_laser_t Expt_laser, const double Time, double*Temperature, std::vector<double>*Grain_temp){

	*Temperature = Expt_laser.Laser_Temp_MIN + (Expt_laser.Laser_Temp_MAX-Expt_laser.Laser_Temp_MIN)*exp( -pow( ((Time-3*Expt_laser.cooling_time)/Expt_laser.cooling_time),2.0 ) );
	for(int grain_num=0;grain_num<Grain_temp->size();grain_num++){Grain_temp->at(grain_num) = *Temperature;}

	return 0;
}

int Temperature_profile_spatial(const int Num_Layers, const Voronoi_t VORO, const Expt_laser_t Expt_laser,
						const double cen_X, const double cen_Y, const double Time, Grain_t*Grain){

	double Temporal_Temp, Spatial_Temp;

	Temporal_Temp = (Expt_laser.Laser_Temp_MAX-Expt_laser.Laser_Temp_MIN)*exp(-pow(((Time-3*Expt_laser.cooling_time)/Expt_laser.cooling_time),2.0));

	for(int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
		double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

		Spatial_Temp = Expt_laser.Laser_Temp_MIN + Temporal_Temp * exp(-(pow(Grain_X-cen_X,2.0)/(2.0*pow(Expt_laser.Tprofile_width_X,2.0)) + pow(Grain_Y-cen_Y,2.0)/(2.0*pow(Expt_laser.Tprofile_width_Y,2.0))));
	 	// Assign temperature to each layer.
	 	for(int Layer=0;Layer<Num_Layers;++Layer){
	 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
	 		Grain->Temp[grain_in_system] = Spatial_Temp;
	 }	}

	return 0;
}

