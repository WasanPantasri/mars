/*
 * HAMR_Write_Data.cpp
 *
 *  Created on: 28 Sep 2018
 *      Author: ewan
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/LLB_import.hpp"
#include "../../hdr/Importers/Experiments/Experiment_HAMR_writing_import.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Solvers/LLB.hpp"
#include "../../hdr/HAMR/Temperature_profile.hpp"
#include "../../hdr/HAMR/Field_profile.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"


#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"

/* This is a simulation for writing data via HAMR. */

int HAMR_write_data(ConfigFile cfg){

	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Expt_timings_t Expt_timings;
	Expt_H_app_t Expt_field;
	Expt_data_write_t Expt_data;
	Expt_laser_t Expt_laser;
	Material_t Materials;
	Interaction_t Int_system;
	LLB_t LLB_data;
	Grain_t Grain;
	Data_bit_t Bit_data;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml;
	int Tot_grains,output_steps,file_writes,Bit=0,File_count=0,Sim_time_predictor=0,
			Tot_bits;
	double temperature, MxT,MyT,MzT,MlT,Temp_norm, Field_MAG, Time=0.0, Profile_Time=0.0,
			Head_X_init, Head_Y_init, Head_X, Head_Y, Profile_tot_time, Tot_time;
	std::string FILENAME_idv;
	std::string Output_switch_file_loc = "Output/HAMR_Grain_switching.dat";
	std::vector<std::string> Output_switch_buffer;

//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	HAMR_writing_expt_import(cfg, "HAMR_write_data",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	LLB_import(cfg, Materials_Config, Structure.Num_layers, &LLB_data);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area, Expt_laser.Laser_Temp_MIN, &Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Materials,Voronoi_data,&Int_system);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###################DETERMINE IMPORTANT VALUES###################//

	Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;
	Tot_bits = Expt_data.Bit_number_X*Expt_data.Bit_number_Y;
	output_steps = round(Expt_timings.Meas_time/LLB_data.dt);
	Field_MAG = Expt_field.H_appl_MAG_min;
// Want these to be assigned from input.
	Head_X_init=Expt_data.Bit_width/2.0+Expt_data.Bit_spacing_X;
	Head_Y_init=Expt_data.Bit_length/2.0+Expt_data.Bit_spacing_Y;
	Head_X=Head_X_init;
	Head_Y=Head_Y_init;

	Profile_tot_time = 6.0*Expt_laser.cooling_time;
	Tot_time = Profile_tot_time*Tot_bits;
	if(Expt_timings.Run_time > Tot_time){file_writes = int(Expt_timings.Run_time/Expt_timings.Meas_time);}
	else{file_writes = int(Tot_time/Expt_timings.Meas_time);}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_switch_buffer);

	std::cout << "Output steps: " << output_steps << std::endl;
	auto start = std::chrono::system_clock::now();

	std::cout << "\n********************\n****\n" << "\tTime required for writing all bits is greater then Run_time!" << "\n\tThe end time is now "
			  << Tot_time << " seconds\n****\n********************\n" << std::endl;

//##########Generate a list of the grains contained in each bit##########//
	double bit_position_X=Head_X_init, bit_position_Y=Head_Y_init;
	int Bit_number=0;
	Bit_data.Bit_grain_list.resize(Tot_bits);
	// Loop over every bit
	for(int Bit_y=0;Bit_y<Expt_data.Bit_number_Y;++Bit_y){
		for(int Bit_x=0;Bit_x<Expt_data.Bit_number_X;++Bit_x){
			bit_position_X += Expt_data.Bit_width+Expt_data.Bit_spacing_X;

			for(int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
				double grain_pos_X=Voronoi_data.Pos_X_final[grain_in_layer];
				double grain_pos_Y=Voronoi_data.Pos_Y_final[grain_in_layer];
				// Check that the grain resides in the bit
				if(grain_pos_X>(bit_position_X-Expt_data.Bit_width/2.0) && grain_pos_X<(bit_position_X+Expt_data.Bit_width/2.0)){
					if(grain_pos_Y>(bit_position_Y-Expt_data.Bit_length/2.0) && grain_pos_Y<(bit_position_Y+Expt_data.Bit_length/2.0)){
						Bit_data.Bit_grain_list[Bit_number].push_back(grain_in_layer);
				}	}
			}
			++Bit_number;
		}
		bit_position_X = Head_X_init;
		bit_position_Y += Expt_data.Bit_length+Expt_data.Bit_spacing_Y;
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	// Simulation will run until all bits are written OR run_time whichever is longer.
	while(Time<Expt_timings.Run_time){

		for(int Bit_y=0;Bit_y<Expt_data.Bit_number_Y;++Bit_y){
			for(int Bit_x=0;Bit_x<Expt_data.Bit_number_X;++Bit_x){

				std::cout << "Head Position: " << Head_X << " , " << Head_Y << std::endl;
				Expt_field.H_appl_unit.z = Expt_data.Writable_data[Bit];

				while(Profile_Time<Profile_tot_time){
					for(int Steps_until_output=0;Steps_until_output<output_steps;Steps_until_output++){
						Temperature_profile_spatial(Structure.Num_layers,Voronoi_data,Expt_laser,Head_X,Head_Y,Profile_Time,&Grain);
						Field_profile_spatial(Structure.Num_layers,Voronoi_data,Expt_field,Expt_laser.cooling_time,Head_X,Head_Y,Profile_Time,LLB_data.dt,&Field_MAG,&Grain);
						LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,Voronoi_data,&LLB_data,&Grain); // Note this integrates the ENTIRE system - need to change this in the future.
						Time += LLB_data.dt; Profile_Time += LLB_data.dt;
					}

					/*
					 * Could do with having layer specific file outputs?
					 */
					FILENAME_idv = std::to_string(File_count) + ".dat";
					Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
					++File_count;

					// Predict simulation run time
					++Sim_time_predictor;
					if(Sim_time_predictor==10){
						auto end = std::chrono::system_clock::now();
						std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
						double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
						std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
					}
				}
				Head_X += Expt_data.Bit_width+Expt_data.Bit_spacing_X; // Moving head takes 0 seconds.
				Profile_Time = 0.0;
				++Bit;
			}
			Head_X = Head_X_init;
			Head_Y += Expt_data.Bit_length+Expt_data.Bit_spacing_Y; // Moving head takes 0 seconds.
		}

		// Continue simulation but no longer write bits.
		for(int Steps=0;Steps<output_steps;Steps++){
			LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,Voronoi_data,&LLB_data,&Grain); // Note this integrates the ENTIRE system - need to change this in the future.
			Time += LLB_data.dt; Profile_Time += LLB_data.dt;
		}
		FILENAME_idv = std::to_string(File_count) + ".dat";
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
		++File_count;
	}
	std::cout << "Bits written: " << Tot_bits << std::endl;
	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_switch_buffer,Output_switch_file_loc);
	return 0;
}



