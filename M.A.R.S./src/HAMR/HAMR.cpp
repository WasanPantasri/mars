/*
 * HAMR.cpp
 *
 *  Created on: 21 Aug 2018
 *      Author: ewan
 */


#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/LLB_import.hpp"
#include "../../hdr/Importers/Experiments/Experiment_HAMR_writing_import.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Solvers/LLB.hpp"
#include "../../hdr/HAMR/Temperature_profile.hpp"
#include "../../hdr/HAMR/Field_profile.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_layer_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"



/* This is a simulation for applying temperature and field profiles to
 * an entire system to investigate their effects. */
int HAMR(ConfigFile cfg){

	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Expt_timings_t Expt_timings;
	Expt_H_app_t Expt_field;
	Expt_data_write_t Expt_data;
	Expt_laser_t Expt_laser;
	Material_t Materials;
	Interaction_t Int_system;
	LLB_t LLB_data;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml,Grain_output_list;
	int Tot_grains,output_steps,file_writes,File_count=0,Sim_time_predictor=0;
	double temperature, MxT,MyT,MzT,MlT,Temp_norm, Field_MAG, Time=0.0;
	std::string FILENAME_idv;

	std::vector<std::string> Output_buffer;
	std::string OUTPUT_switch_loc = "Output/HAMR_Grain_switching.dat";
	std::string OUTPUT = "Output/HAMR_multi_layer_MERGE.dat";
	std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends


//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	HAMR_writing_expt_import(cfg, "HAMR",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	LLB_import(cfg, Materials_Config, Structure.Num_layers, &LLB_data);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius, &Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area, Expt_laser.Laser_Temp_MIN, &Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Materials,Voronoi_data,&Int_system);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	// Generate output list for entire system
	for(int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list[grain]=grain;}
	Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;
	output_steps = round(Expt_timings.Meas_time/LLB_data.dt);
	temperature = Expt_laser.Laser_Temp_MIN;
	Field_MAG = Expt_field.H_appl_MAG_min;

	file_writes = int(Expt_timings.Run_time/Expt_timings.Meas_time);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	std::cout << "Output steps: " << output_steps << std::endl;
	auto start = std::chrono::system_clock::now();
	HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Field_MAG,temperature,Time,Grain_output_list,OUTPUT);
	HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_buffer);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	while(Time<Expt_timings.Run_time){
		for(int Steps=0;Steps<output_steps;Steps++){
			// Determine Temperature and Field.
			Temperature_profile(Expt_laser,Time,&temperature,&Grain.Temp);
			Field_profile(Structure.Num_layers,Voronoi_data,Expt_field,Expt_laser.cooling_time,Time,LLB_data.dt,&Field_MAG,&Grain);
			LL_B(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,Voronoi_data,&LLB_data,&Grain);
			Time += LLB_data.dt;
		}

		FILENAME_idv = std::to_string(File_count) + ".dat";
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
		++File_count;

		// Predict simulation run time
		++Sim_time_predictor;
		if(Sim_time_predictor==10){
			auto end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
			double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
			std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
		}

	}

	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_buffer,OUTPUT_switch_loc);
	return 0;
}
