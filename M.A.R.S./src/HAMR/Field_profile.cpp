/*
 * Field_profile.cpp
 *
 *  Created on: 5 Sep 2018
 *      Author: ewan
 */

#include "math.h"

#include "../../hdr/Structures.hpp"


/* Calculates the Field strength at the specified TIME for a trapezium profile.
 * The profile is such that the field reaches MAX within the "Field_ramp_time"
 * and remains at MAX until "Field_ramp_time" remains for the temperature profile,
 * at which point it falls to "H_app_MAG_min". */
int Field_profile(const int Num_Layers, const Voronoi_t VORO, const Expt_H_app_t Expt_field,
				  const double cooling_time, const double Time, const double Time_increment, double*Field_MAG, Grain_t*Grain){

	Vec3 Applied_Field;

	if(Time<Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
		*Field_MAG += (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}
	else if(Time > 6*cooling_time-Expt_field.Field_ramp_time && Time < 6*cooling_time){
		*Field_MAG -= (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}

	Applied_Field.x = Expt_field.H_appl_unit.x*(*Field_MAG);
	Applied_Field.y = Expt_field.H_appl_unit.y*(*Field_MAG);
	Applied_Field.z = Expt_field.H_appl_unit.z*(*Field_MAG);


	for(int layer=0;layer<Num_Layers;layer++){
		int offset = layer*VORO.Num_Grains;
		for(int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
			int grain_in_system = offset + grain_in_layer;
			Grain->H_appl[grain_in_system].x = Applied_Field.x;
			Grain->H_appl[grain_in_system].y = Applied_Field.y;
			Grain->H_appl[grain_in_system].z = Applied_Field.z;
	}	}

	return 0;
}

int Field_profile_spatial(const int Num_Layers, const Voronoi_t VORO, const Expt_H_app_t Expt_field,
		  const double cooling_time, const double Head_x, const double Head_y,const double Time, const double Time_increment,
		  double*Field_MAG, Grain_t*Grain){

	Vec3 Applied_Field;
	// Keep track of the field profile
	if(Time<Expt_field.Field_ramp_time && *Field_MAG<Expt_field.H_appl_MAG_max){
		*Field_MAG += (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}
	else if(Time > 6*cooling_time-Expt_field.Field_ramp_time && Time < 6*cooling_time){
		*Field_MAG -= (Expt_field.H_appl_MAG_max-Expt_field.H_appl_MAG_min)/Expt_field.Field_ramp_time * Time_increment;
	}

	Applied_Field.x = Expt_field.H_appl_unit.x*(*Field_MAG);
	Applied_Field.y = Expt_field.H_appl_unit.y*(*Field_MAG);
	Applied_Field.z = Expt_field.H_appl_unit.z*(*Field_MAG);

	// Apply field profile only to grains within range of the write head.
	for(int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
		double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];
		if(fabs(Head_x-Grain_X)<=(Expt_field.Field_width_X/2.0) && fabs(Head_y-Grain_Y)<=(Expt_field.Field_width_Y/2.0)){
		 	for(int Layer=0;Layer<Num_Layers;++Layer){
		 		int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
				Grain->H_appl[grain_in_system].x = Applied_Field.x;
				Grain->H_appl[grain_in_system].y = Applied_Field.y;
				Grain->H_appl[grain_in_system].z = Applied_Field.z;
	}	}	}

	return 0;
}


