/*
 * Sigma_Tc.cpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include "math.h"

#include "../hdr/Config_File/ConfigFile_import.hpp"
#include "../hdr/Structures.hpp"
#include "../hdr/Importers/Structure_import.hpp"
#include "../hdr/Importers/Grain_setup.hpp"
#include "../hdr/Importers/Materials_import.hpp"
#include "../hdr/Voronoi.hpp"
#include "../hdr/Solvers/KMC_Solver.hpp"
#include "../hdr/Data_output/HAMR_grain_output.hpp"
#include "../hdr/Importers/Experiments/Experiment_HAMR_long_timescale_import.hpp"
#include "../hdr/Importers/KMC_import.hpp"
#include "../hdr/Interactions/Generate_interactions.hpp"

int Sigma_Tc(const ConfigFile cfg){
	std::cout << "Sigma Tc simulation" << std::endl;

	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Expt_timings_t Expt_timings;
	Expt_H_app_t Expt_field;
	Expt_data_write_t Expt_data;
	Expt_laser_t Expt_laser;
	Material_t Materials;
	std::vector<ConfigFile> Materials_Config;
	Interaction_t Int_system;
	KMC_t KMC_data;
	Grain_t Grain, Grain_BACKUP;

	double time=0.0;
	std::string FILENAME_idv;
	int File_count=0;
	int Laser_stages = 0;	// 0-never applied | 1-Applied now | 2-turned off

	std::ofstream OUTPUT_FILE_in_time, OUTPUT_FILE_final_values, TC_DIST_CHECK_FILE("Output/TC_dist_check.dat");
	TC_DIST_CHECK_FILE << "Grain Tc tot_grains" << std::endl;


//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	HAMR_long_timescale_import(cfg,"Sigma_Tc",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
	KMC_import(cfg,&KMC_data);
	Materials_import(cfg,Structure.Num_layers, &Materials_Config, &Materials);
	Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Expt_laser.Environ_temp,&Grain);
	int Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;
	for(int grain_in_system=0;grain_in_system<Tot_grains;++grain_in_system){
		Grain.H_appl[grain_in_system].x = Expt_field.H_appl_MAG_max*Expt_field.H_appl_unit.x;
		Grain.H_appl[grain_in_system].y = Expt_field.H_appl_MAG_max*Expt_field.H_appl_unit.y;
		Grain.H_appl[grain_in_system].z = Expt_field.H_appl_MAG_max*Expt_field.H_appl_unit.z;
	}
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Materials,Voronoi_data,&Int_system);
	int Iterations = (Expt_laser.Laser_Temp_MAX - Expt_laser.Laser_Temp_MIN)/Expt_laser.Laser_Temp_interval;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

//###########Make a copy of every grain in order to reset the system after each laser pulse##################
	for(int Layer=0;Layer<Structure.Num_layers;++Layer){
		int Offset = Layer*Voronoi_data.Num_Grains;
		for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voronoi_data.Num_Grains;++Grain_in_Layer){
			unsigned int Grain_in_system = Grain_in_Layer+Offset;

			Grain_BACKUP.m.push_back(Grain.m[Grain_in_system]);
			Grain_BACKUP.Easy_axis.push_back(Grain.Easy_axis[Grain_in_system]);
			Grain_BACKUP.Temp.push_back(Grain.Temp[Grain_in_system]);
			Grain_BACKUP.Vol.push_back(Grain.Vol[Grain_in_system]);
			Grain_BACKUP.K.push_back(Grain.K[Grain_in_system]);
			Grain_BACKUP.Tc.push_back(Grain.Tc[Grain_in_system]);
			Grain_BACKUP.Ms.push_back(Grain.Ms[Grain_in_system]);
			Grain_BACKUP.Callen_power.push_back(Grain.Callen_power[Grain_in_system]);
			Grain_BACKUP.Crit_exp.push_back(Grain.Crit_exp[Grain_in_system]);

			Grain_BACKUP.H_appl.resize(Grain.H_appl.size());
			Grain_BACKUP.H_appl[Grain_in_system].x 		= Grain.H_appl[Grain_in_system].x;
			Grain_BACKUP.H_appl[Grain_in_system].y 		= Grain.H_appl[Grain_in_system].y;
			Grain_BACKUP.H_appl[Grain_in_system].z 		= Grain.H_appl[Grain_in_system].z;

	}	}
//###########################################################################################################
	// Ensure write-read time is >= time taken to apply the pulse.
	if(Expt_timings.write_read_time<Expt_timings.Application_time+Expt_timings.Initialisation_time){
		double Minimum_time = Expt_timings.Application_time+Expt_timings.Initialisation_time;
		double New_time = Minimum_time + 0.1*Minimum_time;
		std::cout << "Insufficient write-read time, extending to " << New_time << std::endl;
		Expt_timings.write_read_time = New_time;
	}

	// Output all Tc values so the distribution can be validated.
	for(int grain_in_system=0;grain_in_system<Tot_grains;++grain_in_system){
		TC_DIST_CHECK_FILE << std::setprecision(10) << grain_in_system << " " << Grain.Tc[grain_in_system] << " " << Voronoi_data.Num_Grains << std::endl;
	}
	TC_DIST_CHECK_FILE.close();

	for(int runs=0;runs<Iterations;++runs){
		double Laser_temperature = Expt_laser.Laser_Temp_MIN + runs*Expt_laser.Laser_Temp_interval, Applied_Temp=Expt_laser.Environ_temp;
		std::vector<double> Average_Mx (Structure.Num_layers+1), Average_My (Structure.Num_layers+1),
						    Average_Mz (Structure.Num_layers+1), Probability(Structure.Num_layers+1),SUM_MsV(Structure.Num_layers+1);
		std::string FILENAME_pre_2 = "Temp_" + std::to_string(Laser_temperature) + "_";
		std::cout << std::setprecision(5) << "Pulse temperature = " << Laser_temperature << std::endl;

		// Generate files for time resolved data
		for(int layer=0;layer<=Structure.Num_layers;++layer){
			std::string FILENAME_pre_1 = "Layer_" + std::to_string(layer) + "_";
			OUTPUT_FILE_in_time.open(("Output/" + FILENAME_pre_1 + FILENAME_pre_2 + "Average_M.dat").c_str());
			OUTPUT_FILE_in_time << "Mx My Mz Prob Temp time" << std::endl;
			OUTPUT_FILE_in_time.close();
		}

		while(time<Expt_timings.write_read_time){
			double SUM_MsV_all_layers=0.0, Mx_all_layers=0.0, My_all_layers=0.0, Mz_all_layers=0.0, Prob_all_layers=0.0;
//##############################APPLY LASER PULSE##############################//
			if(time>Expt_timings.Initialisation_time && Laser_stages==0){
				for(int grain_in_system=0;grain_in_system<Tot_grains;++grain_in_system){Grain.Temp[grain_in_system] = Laser_temperature;}
				Applied_Temp = Laser_temperature;
				++Laser_stages;
			}
			else if(time>Expt_timings.Initialisation_time+Expt_timings.Application_time && Laser_stages==1){
				for(int grain_in_system=0;grain_in_system<Tot_grains;++grain_in_system){Grain.Temp[grain_in_system] = Expt_laser.Environ_temp;}
				Applied_Temp = Expt_laser.Environ_temp;
				++Laser_stages;
			}
			KMC_solver(Voronoi_data.Num_Grains,Structure.Num_layers,Int_system,KMC_data.time_step,KMC_data.ZeroKInputs,&Grain,"Sigma_Tc");
			time += KMC_data.time_step;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##############################DETERMINE AVERAGE MAGNETISATIONS##############################//
			// Experimentally they measure Ms*V, thus the output needs to match that.
			// Determine normalisation values.
			for(int layer=0;layer<Structure.Num_layers;++layer){
				double dummy_SUM_MsV=0.0, offset=Voronoi_data.Num_Grains*layer;
				for(int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
					int grain_in_system = grain_in_layer+offset;
					dummy_SUM_MsV += Grain.Ms[grain_in_system]*Grain.Vol[grain_in_system];
				}
				SUM_MsV[layer]=dummy_SUM_MsV;
				SUM_MsV_all_layers+=SUM_MsV[layer];
			}
			SUM_MsV.back() = SUM_MsV_all_layers;

			// Determine average magnetisations.
			for(int layer=0;layer<Structure.Num_layers;++layer){
				double offset=Voronoi_data.Num_Grains*layer;
				Average_Mx[layer]=Average_My[layer]=Average_Mz[layer]=Probability[layer]=0.0;
				for(int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
					int grain_in_system = grain_in_layer+offset;
					Average_Mx[layer]  += Grain.Ms[grain_in_system]*Grain.Vol[grain_in_system]*Grain.m[grain_in_system].x;
					Average_My[layer]  += Grain.Ms[grain_in_system]*Grain.Vol[grain_in_system]*Grain.m[grain_in_system].y;
					Average_Mz[layer]  += Grain.Ms[grain_in_system]*Grain.Vol[grain_in_system]*Grain.m[grain_in_system].z;
					Probability[layer] += (Grain.m[grain_in_system].z/fabs(Grain.m[grain_in_system].z));
				}
				Mx_all_layers      += Average_Mx[layer];
				My_all_layers      += Average_My[layer];
				Mz_all_layers      += Average_Mz[layer];
				Prob_all_layers    += Probability[layer];
				Average_Mx[layer]  /= SUM_MsV[layer];
				Average_My[layer]  /= SUM_MsV[layer];
				Average_Mz[layer]  /= SUM_MsV[layer];
				Probability[layer] /= Voronoi_data.Num_Grains;

			}
			// Full system values
			Average_Mx.back() = Mx_all_layers/SUM_MsV.back();
			Average_My.back() = My_all_layers/SUM_MsV.back();
			Average_Mz.back() = Mz_all_layers/SUM_MsV.back();
			Probability.back() = Prob_all_layers/Tot_grains;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

			// OUTPUT for time resolved data
			for(int layer=0;layer<=Structure.Num_layers;++layer){
				std::string FILENAME_pre_1 = "Layer_" + std::to_string(layer) + "_";
				OUTPUT_FILE_in_time.open(("Output/" + FILENAME_pre_1 + FILENAME_pre_2 + "Average_M.dat").c_str(),std::ofstream::app);
				OUTPUT_FILE_in_time << Average_Mx[layer] << " " << Average_My[layer]  << " " << Average_Mz[layer]  << " " << Probability[layer]  << " " << Applied_Temp << " " << time << std::endl;
				OUTPUT_FILE_in_time.close();
			}
			++File_count;
		}

		// Output for thermo-remanence data
		for(int layer=0;layer<=Structure.Num_layers;++layer){
			std::string FILENAME_pre_1 = "Layer_" + std::to_string(layer) + "_";
			OUTPUT_FILE_final_values.open(("Output/" + FILENAME_pre_1 + "Final_magnetisation.dat").c_str(),std::ofstream::app);
			OUTPUT_FILE_final_values << Average_Mx[layer] << " " << Average_My[layer]  << " " << Average_Mz[layer]  << " " << Probability[layer]  << " " <<  Laser_temperature << " " << time << std::endl;
			OUTPUT_FILE_final_values.close();
		}
		// Output final system configuration
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,time,".dat",FILENAME_pre_2+"Final_config_");
		// Reset system
		time = 0.0; Laser_stages=0; File_count=0;

		//###############################Reset the system back to initial conditions#################################
		for(int Layer=0;Layer<Structure.Num_layers;++Layer){
			int Offset = Layer*Voronoi_data.Num_Grains;
			for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Voronoi_data.Num_Grains;++Grain_in_Layer){
				unsigned int Grain_in_system = Grain_in_Layer+Offset;
				Grain.m[Grain_in_system]         		= Grain_BACKUP.m[Grain_in_system];
				Grain.Easy_axis[Grain_in_system] 		= Grain_BACKUP.Easy_axis[Grain_in_system];
				Grain.Temp[Grain_in_system]      		= Grain_BACKUP.Temp[Grain_in_system];
				Grain.Vol[Grain_in_system] 				= Grain_BACKUP.Vol[Grain_in_system];
				Grain.K[Grain_in_system] 				= Grain_BACKUP.K[Grain_in_system];
				Grain.Tc[Grain_in_system] 				= Grain_BACKUP.Tc[Grain_in_system];
				Grain.Ms[Grain_in_system] 				= Grain_BACKUP.Ms[Grain_in_system];
				Grain.Callen_power[Grain_in_system] 	= Grain_BACKUP.Callen_power[Grain_in_system];
				Grain.Crit_exp[Grain_in_system] 		= Grain_BACKUP.Crit_exp[Grain_in_system];
				Grain.H_appl[Grain_in_system].x 		= Grain_BACKUP.H_appl[Grain_in_system].x;
				Grain.H_appl[Grain_in_system].y 		= Grain_BACKUP.H_appl[Grain_in_system].y;
				Grain.H_appl[Grain_in_system].z 		= Grain_BACKUP.H_appl[Grain_in_system].z;
		}	};
		//###########################################################################################################
	}
	return 0;
}

