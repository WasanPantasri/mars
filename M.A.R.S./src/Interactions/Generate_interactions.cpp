/*
 * Neighbour_list_generator.cpp
 *
 *  Created on: 8 Feb 2019
 *      Author: Ewan Rannala
 */

#include <vector>
#include "math.h"
#include <iostream>

#include "../../hdr/Globals.hpp"
#include "../../hdr/Distributions_generator.hpp"
#include "../../hdr/Structures.hpp"


/* Requires
 * 		From materials:	z, J_Dist_Type, StdDev_J, H_sat, Hexch_str_out_plane_UP, Hexch_str_out_plane_DOWN
 * 		From voronoi: Num_Grains, Geo_grain_centre_X, Geo_grain_centre_Y, Centre_X, Centre_Y, x_max, y_max,
 *					  Neighbour_final, Average_area, Grain_Area, Contact_lengths, Average_contact_length
 */

int Generate_interactions(const int Num_layers,const std::vector<double> Grain_Vol,
		const double Magneto_CUT_OFF,const Material_t MAT,const Voronoi_t VORO,Interaction_t*Int_system){

	std::cout << "Generating interactions..." << std::endl;
	int Tot_grains = VORO.Num_Grains*Num_layers;
	// Set size of interaction vectors
	Int_system->Magneto_neigh_list.resize(Tot_grains);
	Int_system->Exchange_neigh_list.resize(Tot_grains);
	Int_system->Wxx.resize(Tot_grains); Int_system->Wxy.resize(Tot_grains);
	Int_system->Wxz.resize(Tot_grains);	Int_system->Wyy.resize(Tot_grains);
	Int_system->Wyz.resize(Tot_grains);	Int_system->Wzz.resize(Tot_grains);
	Int_system->H_exch_str.resize(Tot_grains);


//################### MAGNETOSTATIC ########################//
	// look through all grains in the ENTIRE system
	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = (i+1) % VORO.Num_Grains,
		    Z_layer_i = int(i/VORO.Num_Grains);
		double Grain_x=VORO.Geo_grain_centre_X[grain_in_layer_i];
		double Grain_y=VORO.Geo_grain_centre_Y[grain_in_layer_i];
		for(int j=0;j<Tot_grains;++j){	// CAN MAYBE USE VORONOI magneto_neigh LIST TO REDUCE COMPUTATIONAL COST
			if(i==j){/*Prevent self-interaction*/}
			else{
				int grain_in_layer_j = (j+1) % VORO.Num_Grains,
					Z_layer_j = int(j/VORO.Num_Grains);
				double Neigh_x=VORO.Geo_grain_centre_X[grain_in_layer_j];
				double Neigh_y=VORO.Geo_grain_centre_Y[grain_in_layer_j];
				double x_diff, y_diff, z_diff, Separation;
				// Account for the periodic boundaries of the system.
	  			if((Grain_x-Neigh_x) > (VORO.Centre_X)){Neigh_x += VORO.x_max;}					// LHS
	  			else if((Grain_x-Neigh_x) < (-VORO.Centre_X)){Neigh_x -= VORO.x_max;}			// RHS
	  			if((Grain_y-Neigh_y) < (-VORO.Centre_Y)){Neigh_y -= VORO.y_max;}				// TOP
	  			else if((Grain_y-Neigh_y) > (VORO.Centre_Y)){Neigh_y += VORO.y_max;}			// BOT

				x_diff = (Neigh_x-Grain_x);
				y_diff = (Neigh_y-Grain_y);
				z_diff = MAT.z[Z_layer_j]-MAT.z[Z_layer_i];
				Separation = sqrt(x_diff*x_diff+y_diff*y_diff+z_diff*z_diff);

				if(Separation<=Magneto_CUT_OFF){
					Int_system->Magneto_neigh_list[i].push_back(j);
		  			double Wprefactor = (Grain_Vol[grain_in_layer_j]*1e-21)/(4*PI*pow(Separation,3));
		  			// Symmetric matrix, thus only 6/9 elements are determined.
		  			Int_system->Wxx[i].push_back(Wprefactor*(((3*x_diff*x_diff)/(Separation*Separation))-1));
		  			Int_system->Wxy[i].push_back(Wprefactor*((3*x_diff*y_diff)/(Separation*Separation)));
		  			Int_system->Wxz[i].push_back(Wprefactor*((3*x_diff*z_diff)/(Separation*Separation)));
		  			Int_system->Wyy[i].push_back(Wprefactor*(((3*y_diff*y_diff)/(Separation*Separation))-1));
		  			Int_system->Wyz[i].push_back(Wprefactor*((3*y_diff*z_diff)/(Separation*Separation)));
		  			Int_system->Wzz[i].push_back(Wprefactor*(((3*z_diff*z_diff)/(Separation*Separation))-1));
	}	}	}	}


//################### EXCHANGE ########################//
	int counter=0;
	double Average=0.0, Normaliser;
	std::vector<std::vector<double>> Product(Tot_grains),exchange_constants(Tot_grains);
	// PERFORM NORMALISATION OF THE EXCHANGE DISTRIBUTION
	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = (i+1) % VORO.Num_Grains,
		    Z_layer_i = int(i/VORO.Num_Grains);
		// Generate random numbers for neighbours of grain i.
		Dist_gen(VORO.Neighbour_final[grain_in_layer_i].size(),MAT.J_Dist_Type[Z_layer_i],1.0,MAT.StdDev_J[Z_layer_i],&exchange_constants[i]);

		for(int j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			double Avg_A_o_A = (VORO.Average_area/VORO.Grain_Area[grain_in_layer_i]);
			double L_o_Avg_L = (VORO.Contact_lengths[grain_in_layer_i][j]/VORO.Average_contact_length);
			Product[i].push_back(exchange_constants[i][j]*Avg_A_o_A*L_o_Avg_L);
	}	}

	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = (i+1) % VORO.Num_Grains;
		for(int j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			counter++;
			Average += Product[i][j];
	}	}
	Average /= counter;
	Normaliser = 1.0/Average;

	// CREATE NEIGHBOUR LIST PLUS EXCHANGE VALUES IN-PLANE AND OUT-PLANE
	for(int i=0;i<Tot_grains;++i){
		int grain_in_layer_i = (i+1) % VORO.Num_Grains,
			Z_layer_i = int(i/VORO.Num_Grains),
			Layer_offset = Z_layer_i*VORO.Num_Grains; // Enables use of single layer vector for all layers
		for(int j=0;j<VORO.Neighbour_final[grain_in_layer_i].size();++j){
			Int_system->Exchange_neigh_list[i].push_back(VORO.Neighbour_final[grain_in_layer_i][j]+Layer_offset);
			Int_system->H_exch_str[i].push_back(Product[i][j]*Normaliser*MAT.H_sat[Z_layer_i]);
		}
		// Conditions to check for layers above and below
		if(Z_layer_i-1>=0){ // Add exchange from layer below
			Int_system->Exchange_neigh_list[i].push_back(i+VORO.Num_Grains); // Add ith grain from upper layer
			double EXCH_FROM_DN = MAT.Hexch_str_out_plane_UP[Z_layer_i-1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
			Int_system->H_exch_str[i].push_back(EXCH_FROM_DN);
		}
		if(Z_layer_i+1<=Num_layers-1){ // Add exchange from layer above
			Int_system->Exchange_neigh_list[i].push_back(i-VORO.Num_Grains); // Add ith grain from lower layer
			double EXCH_FROM_UP = MAT.Hexch_str_out_plane_DOWN[Z_layer_i+1]*VORO.Grain_Area[grain_in_layer_i]/VORO.Average_area;
			Int_system->H_exch_str[i].push_back(EXCH_FROM_UP);
	}	}
	return 0;
}


