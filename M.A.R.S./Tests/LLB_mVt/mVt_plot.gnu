set term postscript eps color enhanced font "Times-Roman,25"
set output "LLB_mVt_test.eps"

set ylabel "Magnetisation length"
set xlabel "Temperature (K)"

datafile = "Output/LLB_mVt.dat"


p datafile u 5:4 w l t 'Tc=700K'
