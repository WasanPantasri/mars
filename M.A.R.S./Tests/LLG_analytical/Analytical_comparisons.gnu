set term postscript eps color enhanced font "Times-Roman,25"
set output "LLG_analytical_test.eps"
set key outside

set ylabel "Magnetisation"
set xlabel "Time (ns)"
set xrange [0:2e-09]
set mxtics 2
set mytics 2
set grid xtics ytics mxtics mytics
H = 400*3.1415926535
G = 1.760859644e+7
f(x,alpha) = 1.0/(cosh((alpha*H*G*x)/(1+alpha**2)))*cos((G*H*x)/(1+alpha**2))
g(x,alpha) = 1.0/(cosh((alpha*H*G*x)/(1+alpha**2)))*sin((G*H*x)/(1+alpha**2))
h(x,alpha) = tanh((alpha*G*H*x)/(1+alpha**2))
datafile = "Output/LLG.dat"
set size square

set ytics format "%1.1f"
set xtics ("0.0" 0, "0.5" 5e-10, "1.0" 1e-9, "1.5" 1.5e-9, "2.0" 2.0e-9)
show key

p datafile u 4:1 t "x"  with line lt rgb "#00FFFF" lw 2,\
  datafile u 4:2 t "y"  with line lt rgb "#0000FF" lw 2,\
  datafile u 4:3 t "z"  with line lt rgb "#7FFF00" lw 2,\
  f(x,0.1) t "a_x" with line dashtype 5 lt rgb "#FF1493" lw 4,\
  g(x,0.1) t "a_y" with line dashtype 6 lt rgb "#00FF7F" lw 4,\
  h(x,0.1) t "a_z" with line dashtype 7 lt rgb "#FF4500" lw 4
