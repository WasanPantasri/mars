set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

f0 = "Output/LLG.dat"
norm1200=0.0370025853263
set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (rad)"
set xrange[0:2]
set yrange[0:]
set output "LLG_Boltzmann_test.eps"
#2.1651e-25
boltz(x,K,V,T) =sin(x)*exp(-(K*V*sin(x)*sin(x))/(1.38e-16*T))
p f0 u ($1+0.005):($2/$4) smooth freq with boxes title "Simulation data T=1200K",\
boltz(x,4.2e+6,554.256e-21,1200)/norm1200 w l title "Boltzmann distribution: T=1200K"
