set term postscript eps color enhanced font "Times-Roman,25"
set output "LLG_hysteresis_test.eps"
unset key

set ylabel "M.H"
set xlabel "H_{appl}/H_{k}"
set xrange [-1.5:1.5]
set yrange [-1.1:1.1]
set grid xtics ytics mxtics mytics

f0 = "Output/LLG.dat"


p f0 i 0 u ($10/$11):($12) w l t "89.9 Deg",\
  '' i 1 u ($10/$11):($12) w l t "75 Deg",\
  '' i 2 u ($10/$11):($12) w l t "60 Deg",\
  '' i 3 u ($10/$11):($12) w l t "45 Deg",\
  '' i 4 u ($10/$11):($12) w l t "30 Deg",\
  '' i 5 u ($10/$11):($12) w l t "15 Deg",\
  '' i 6 u ($10/$11):($12) w l t "0.01 Deg",\
