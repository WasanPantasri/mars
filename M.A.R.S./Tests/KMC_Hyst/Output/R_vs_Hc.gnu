K  = 5.0e+6 
Ms = 800.0
Hk = 2*K/Ms
f0= 10.0**(9)
Temp=100.0
V = 554.256258422042*1e-21
Kb = 1.3806485279e-16
beta(T) = K*V/(Kb*T)
Hc(x) = x 
h(x) = Hc(x)/Hk
a(x) = 1.0 - h(x)


set terminal pdf enhanced color  font "Bold-Times-Roman,20" size 10,5
set key bottom right #at 1600,15

set output "../R_f_Hc_1_10K.pdf"
set ylabel "H_c/H_K"  
set xlabel "Field sweep rate (Oe/s)"  font "Bold-Times-Roman,20"
set key font"Times-Roman,25"
set title"R= f(H_c)"  font "Times-Roman,25"

set xrange [10.0**(-4):10.0**17]

set title ""


do for [Temp=100:700:100]{
	log_R(x,T) =  -1*( beta(T)*a(x)**2 + log( a(x) ) - log( Hk*f0/(2*beta(T)) ) ) #ln(R) = f(Hc)
	set table "theoretical2_".Temp.".dat"
	p [0:Hk] exp(log_R(x,Temp))
	unset table
}

system("for i in 100 200 300 400 500 600 700; do head -n -2 theoretical2_$i.dat > theoretical_$i.dat; done")
!rm theoretical2_*

set logscale x 
p for [Temp=100:700:100] "theoretical_".Temp.".dat" u ($2):($1/Hk) title "" w l lc Temp/100 lw 2.0,\
  for [Temp=100:700:100] "KMC_Hc.dat" i (Temp-100)/100 u (($1)):(($4-$3)/(2*Hk)) title "Simulation T=".Temp."K" w points pt 7 lc Temp/100 
