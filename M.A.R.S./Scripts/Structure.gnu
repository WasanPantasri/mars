unset key

set view equal xy

f0 = "../Output/POS_FILE.dat"
f1 = "../Output/VERT_FILE.dat"
f2 = "../Output/GEO_FILE.dat"
p0 = "../Output/Pre_shrink/POS_FILE.dat"
p1 = "../Output/Pre_shrink/VERT_FILE.dat"
p2 = "../Output/Pre_shrink/GEO_FILE.dat"

set xrange [0:X]
set yrange [0:Y]

p f0 u ($1):($2):0 w labels, f1 u ($1):($2) w l, f2 u ($1):($2) w p, p0 u ($1):($2):0 w labels, p1 u ($1):($2) w l, p2 u ($1):($2) w p
#p f1 u ($1):($2) w l

pause -1
