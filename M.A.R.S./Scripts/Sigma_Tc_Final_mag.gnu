set term postscript eps color enhanced font "Times-Roman,25"


set output "M_vs_T_DOT.eps"
f0 = "../Output/Final_magnetisation.dat"

set title sprintf("{/Symbol s}T_c = %5.2f | {/Symbol s}K = %5.2f | {/Symbol s}V = %5.2f | {/Symbol s}EA = %5.2f",SigmaTc,SigmaK,SigmaV,SigmaEA)
set xlabel "Temperature (K)"
set ylabel "m_z" 
set yrange [-1.05:1.05]
set grid

p f0 u ($4):($3) w lp pt 3 t ''

set output "../Output/M_vs_T_LINE.eps"
p f0 u ($4):($3) w l t ''
