#!/usr/bin/gnuplot

f0 = "../Output/AREA_FILE.dat"

#bin_width = 1;
set boxwidth bin_width
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + bin_width/2.0 )



#sigma = 6.2
#mu = 21.6506
gauss(x)=1.0/(sigma*sqrt(2.*pi))*exp(-(x-mu)**2./(2.*sigma**2))
mulg = log(mu/sqrt(1.0+((sigma*sigma)/(mu*mu))))
sigmalg    = sqrt(log(1.0+(sigma*sigma)/(mu*mu)))
galton(x)=(1.0/(x*sigmalg*sqrt(2.*pi)))*exp(-((log(x)-mulg)**2)/(2.*sigmalg**2))


plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes, gauss(x), galton(x)
#, galton(x)
#p gauss(x), galton(x)
#p galton(x)

pause -1
