/* Distributions_generator.hpp
 *  Created on: 31 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef DISTRIBUTIONS_GENERATOR_HPP_
#define DISTRIBUTIONS_GENERATOR_HPP_

#include <random>
#include <vector>

extern int Dist_gen(const unsigned int, const std::string, const double, const double, std::vector<double>*);

#endif /* DISTRIBUTIONS_GENERATOR_HPP_ */
