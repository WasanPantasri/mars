/* Structures.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef STRUCTURES_HPP_
#define STRUCTURES_HPP_

#include <vector>
#include <string>

extern double PI, KB;

struct Vec3{

	double x,y,z;

};

struct Grain_t {

	std::vector<Vec3> m, Easy_axis;
	std::vector<double> Temp, Vol, K, Tc, Ms, Callen_power, Crit_exp;
	std::vector<Vec3> H_appl;

};

struct Material_t {

	std::vector<std::string> Mag_Type;
	std::vector<Vec3> Initial_mag, Initial_anis;
	std::vector<std::string> Type, Tc_Dist_Type, K_Dist_Type, J_Dist_Type;
	std::vector<double> Ms, Avg_Tc, StdDev_Tc, Avg_K, StdDev_K, StdDev_J, z, dz, Anis_angle, H_sat,
						Easy_axis_polar, Easy_axis_azimuth, Callen_power, Crit_exp;
	// Out of plane exchange
	std::vector<double> Hexch_str_out_plane_UP, Hexch_str_out_plane_DOWN;

};

struct Structure_t {

	double Dim_x, Dim_y, Grain_width, Packing_fraction, StdDev_grain_pos,Magneto_Interaction_Radius;
	int Num_layers;
	std::string Magnetostatics_gen_type;

};

struct Voronoi_t {

	unsigned int Num_Grains;
	double  x_max, y_max, Int_Rad, Centre_X, Centre_Y;
	double Average_area=0.0,Average_contact_length=0.0, Vx_MIN=0.0, Vx_MAX=0.0, Vy_MIN=0.0, Vy_MAX=0.0;
	std::vector<double> Pos_X_final, Pos_Y_final, Geo_grain_centre_X, Geo_grain_centre_Y, Grain_Area;
	std::vector<std::vector<double>> Contact_lengths, Vertex_X_final, Vertex_Y_final;
	std::vector<std::vector<int>> Neighbour_final, Magnetostatic_neighbours;

};

struct LLG_t {

	double Gamma=1.760859644e+7, dt;
	std::vector<double> Alpha;

};

struct LLB_t {

	double Gamma=1.760859644e+7, dt;
	std::vector<double> Alpha, Chi_para, Chi_perp, Alpha_PARA, Alpha_PERP, m_EQ;

};

struct KMC_t {

	double time_step;
	bool ZeroKInputs;
};

struct Interaction_t {

	std::vector<std::vector<int>> Magneto_neigh_list, Exchange_neigh_list;
	std::vector<std::vector<double>> Wxx,Wxy,Wxz,Wyy,Wyz,Wzz;
	std::vector<std::vector<double>> H_exch_str;

};

struct Expt_timings_t {

	double Equilibration_time, Run_time, Meas_time,
		   Initialisation_time,Application_time,runoff_time,
		   Cooling_time_p_K;
};

struct Expt_H_app_t {

	double H_appl_MAG_min,H_appl_MAG_max,Field_ramp_time,
		   Field_width_X, Field_width_Y;
	Vec3 H_appl_unit;

};

struct Expt_data_write_t {

	int Bit_number_X, Bit_number_Y;
	double Bit_width, Bit_length,Bit_spacing_X, Bit_spacing_Y;
	std::string Data_Binary;
	std::vector<int> Writable_data;

};

struct Expt_laser_t {

	   double Laser_Temp_MIN,Laser_Temp_MAX,Laser_Temp_interval,Environ_temp,
	   	      Tprofile_width_X, Tprofile_width_Y,cooling_time;

};

struct Data_bit_t {

	std::vector<std::vector<double>> Bit_grain_list;

};

#endif /* STRUCTURES_HPP_ */
