/* Experiment_import.hpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef EXPERIMENT_IMPORT_HPP_
#define EXPERIMENT_IMPORT_HPP_

#include <string>

#include "../../../hdr/Structures.hpp"
#include "../../../hdr/Config_File/ConfigFile_import.hpp"

extern int HAMR_writing_expt_import(const ConfigFile,const std::string,Expt_data_write_t*,Expt_laser_t*,
									Expt_H_app_t*,Expt_timings_t*);

#endif /* EXPERIMENT_IMPORT_HPP_ */
