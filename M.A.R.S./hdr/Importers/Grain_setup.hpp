/* Grain_setup.hpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef GRAIN_SETUP_HPP_
#define GRAIN_SETUP_HPP_

#include <string>
#include "../../hdr/Structures.hpp"

extern int Grain_setup(const unsigned int,const int,const Material_t,const std::vector<double>,const double,Grain_t*);

#endif /* GRAIN_SETUP_HPP_ */
