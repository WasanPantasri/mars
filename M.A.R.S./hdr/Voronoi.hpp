/* Voronoi_2.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef VORONOI_2_HPP_
#define VORONOI_2_HPP_

#include "../hdr/Structures.hpp"

extern int Voronoi(const Structure_t, const double, Voronoi_t*);

#endif /* VORONOI_2_HPP_ */
