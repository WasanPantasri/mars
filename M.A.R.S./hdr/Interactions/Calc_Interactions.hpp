/*
 * Calc_Interactions.hpp
 *
 *  Created on: 23 Aug 2018
 *      Author: ewan
 */

#ifndef CALC_INTERACTIONS_HPP_
#define CALC_INTERACTIONS_HPP_

#include "../../hdr/Structures.hpp"

extern int Calc_Interactions(const Structure_t,const Voronoi_t,const Material_t,Interaction_t*);

#endif /* CALC_INTERACTIONS_HPP_ */
