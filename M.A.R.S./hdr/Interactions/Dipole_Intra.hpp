/* Dipole.hpp
 *  Created on: 15 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef DIPOLE_HPP_
#define DIPOLE_HPP_

#include "../../hdr/Structures.hpp"

extern int Dipole_intra(const Voronoi_t,const double,const int,Interaction_t*);

#endif /* DIPOLE_HPP_ */
