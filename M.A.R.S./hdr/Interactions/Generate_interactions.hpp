/*
 * Interactions_and_neighbours.hpp
 *
 *  Created on: 9 Feb 2019
 *      Author: Ewan Rannala
 */

#ifndef INTERACTIONS_AND_NEIGHBOURS_HPP_
#define INTERACTIONS_AND_NEIGHBOURS_HPP_

#include "../../hdr/Structures.hpp"

extern int Generate_interactions(const int Num_layers,const std::vector<double> Grain_Vol,
		const double Magneto_CUT_OFF,const Material_t MAT,const Voronoi_t VORO,Interaction_t*Int_system);

#endif /* INTERACTIONS_AND_NEIGHBOURS_HPP_ */
