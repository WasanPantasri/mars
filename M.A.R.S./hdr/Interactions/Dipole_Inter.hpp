/*
 * Dipole_Inter.hpp
 *
 *  Created on: 22 Aug 2018
 *      Author: ewan
 */

#ifndef DIPOLE_INTER_HPP_
#define DIPOLE_INTER_HPP_

#include "../../hdr/Structures.hpp"

extern int Dipole_Inter(const Voronoi_t, const double, const double, const double,const int, Interaction_t*);

#endif /* DIPOLE_INTER_HPP_ */
