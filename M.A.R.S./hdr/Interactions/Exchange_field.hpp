/* Exchange_field.hpp
 *  Created on: 3 Aug 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef EXCHANGE_FIELD_HPP_
#define EXCHANGE_FIELD_HPP_

#include <string>
#include "../../hdr/Structures.hpp"

int Exchange_field(const double,const std::string,const double,const Voronoi_t,const int,Interaction_t*);

#endif /* EXCHANGE_FIELD_HPP_ */
