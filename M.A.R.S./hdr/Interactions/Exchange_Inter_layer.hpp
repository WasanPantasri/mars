/*
 * Exchnage_Inter_layer.hpp
 *
 *  Created on: 23 Aug 2018
 *      Author: ewan
 */

#ifndef EXCHNAGE_INTER_LAYER_HPP_
#define EXCHNAGE_INTER_LAYER_HPP_

extern int Exchange_intr_layer(const int,const std::vector<double>,const double,const double,const int,Interaction_t*);

#endif /* EXCHNAGE_INTER_LAYER_HPP_ */
