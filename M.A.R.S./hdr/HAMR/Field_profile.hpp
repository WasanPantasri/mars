/*
 * Field_profile.hpp
 *
 *  Created on: 5 Sep 2018
 *      Author: ewan
 */

#ifndef FIELD_PROFILE_HPP_
#define FIELD_PROFILE_HPP_

#include "../../hdr/Structures.hpp"

extern int Field_profile(const int,const Voronoi_t,const Expt_H_app_t,const double,const double,const double,double*,Grain_t*);

extern int Field_profile_spatial(const int,const Voronoi_t,const Expt_H_app_t,const double,const double,const double,const double,const double,
		double*,Grain_t*);

#endif /* FIELD_PROFILE_HPP_ */
