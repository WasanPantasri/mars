/* ConfigFile_import.hpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef CONFIGFILE_IMPORT_HPP_
#define CONFIGFILE_IMPORT_HPP_

#include <string>
#include <map>

#include "../../hdr/Config_File/Convert.hpp"

class ConfigFile
{
private:
	std::map<std::string, std::string> contents;
	std::string FileName;
	const std::string BoldRedFont = "\033[1;4;31m";
	const std::string ResetFont = "\033[0m";

	void removeComment(std::string&) const;

	void lowerCase(std::string &) const;

	bool onlyWhitespace(const std::string&) const;

	void FormatVector(std::string &, size_t const) const;

	bool validLine(const std::string&) const;

	void extractKey(std::string&, size_t const&, const std::string&) const;

	void extractValue(std::string&, size_t const&, const std::string&) const;

	void extractContents(const std::string&);

	void parseLine(const std::string&, size_t const);

	void ExtractKeys();

public:
	ConfigFile(const std::string&);

	bool keyExists(const std::string&) const;

	template <typename ValueType>
	ValueType getValueOfKey(const std::string&) const;

};



#endif /* CONFIGFILE_IMPORT_HPP_ */
