/*
 * HAMR_switching_output.hpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

#ifndef HAMR_SWITCHING_OUTPUT_HPP_
#define HAMR_SWITCHING_OUTPUT_HPP_

#include "../../hdr/Structures.hpp"

extern int HAMR_Switching_BUFFER(const int,const Voronoi_t,const Grain_t,const std::vector<double>,std::vector<std::string>*);

extern int HAMR_Switching_OUTPUT(const int,const int,const Grain_t,const std::vector<std::string>,const std::string);


#endif /* HAMR_SWITCHING_OUTPUT_HPP_ */
