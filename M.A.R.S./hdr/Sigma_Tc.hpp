/*
 * Sigma_Tc.hpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

#ifndef SIGMA_TC_HPP_
#define SIGMA_TC_HPP_

extern int Sigma_Tc(const ConfigFile cfg);

#endif /* SIGMA_TC_HPP_ */
