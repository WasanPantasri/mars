/*
 * System_generate.hpp
 *
 *  Created on: 5 Sep 2018
 *      Author: ewan
 */

#ifndef SYSTEM_GENERATE_HPP_
#define SYSTEM_GENERATE_HPP_

#include "../hdr/Structures.hpp"

extern int Gen_sys(ConfigFile cfg);

#endif /* SYSTEM_GENERATE_HPP_ */
