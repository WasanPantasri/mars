/*
 * TEST_KMC_SW.hpp
 *
 *  Created on: 19 Nov 2018
 *      Author: Ewan Rannala
 */

#ifndef TEST_KMC_SW_HPP_
#define TEST_KMC_SW_HPP_

extern int KMC_SW_test();

#endif /* TEST_KMC_SW_HPP_ */
