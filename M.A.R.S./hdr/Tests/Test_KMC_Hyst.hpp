/*
 * TEST_KMC_SW.hpp
 *
 *  Created on: 27 Nov 2018
 *      Author: Ewan Rannala
 */

#ifndef TEST_KMC_HYST_HPP_
#define TEST_KMC_HYST_HPP_

extern int KMC_Hyst_test();

#endif /* TEST_KMC_HYST_HPP_ */
