/* Test_LLG_boltz.hpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef TEST_LLG_BOLTZ_HPP_
#define TEST_LLG_BOLTZ_HPP_

extern int LLG_boltz_test();

#endif /* TEST_LLG_BOLTZ_HPP_ */
