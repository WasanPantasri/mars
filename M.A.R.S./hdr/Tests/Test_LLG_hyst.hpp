/* Test_LLG_hyst.hpp
 *  Created on: 23 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef TEST_LLG_HYST_HPP_
#define TEST_LLG_HYST_HPP_

#include "../../hdr/Structures.hpp"

extern double DotP(const Vec3, const Vec3);

extern int LLG_hyst_test();

#endif /* TEST_LLG_HYST_HPP_ */
