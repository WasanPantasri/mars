/* LLG_analyt_test.hpp
 *  Created on: 22 May 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef LLG_ANALYT_TEST_HPP_
#define LLG_ANALYT_TEST_HPP_

extern int LLG_analyt_test();

#endif /* LLG_ANALYT_TEST_HPP_ */
