/* LLB.hpp
 *  Created on: 13 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

#ifndef LLB_HPP_
#define LLB_HPP_

#include "../../hdr/Structures.hpp"

extern int LL_B(const int,const int,const Interaction_t,const Voronoi_t,LLB_t*,Grain_t*);

#endif /* LLB_HPP_ */
