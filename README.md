#MARS - Models of Advanced Recording Systems  
A general use code for performing micromagnetic simulations of magnetic recording media systems.  

###Prerequisites  
g++ version 5.4.0 or above.  

###Installation  
No installation is necessary, merely clone the repository and use the provided bash scripts to run MARS.  

###How to use  
Here we refer to the installation directory as **$MARS**.  
MARS utilises multiple input configuration files for system parameters.
The main file is called **_MARS_input.cfg_**, located within **$MARS/M.A.R.S./Input**.  
There is also an optional input file for the W-matrix of a pregenerated system, this should be named **_W_matrix_MARS.dat_**.  
Each material also requires an input file, the inputs should be placed within **$MARS/M.A.R.S./Input/Materials**.  
If simulating the process of writing bits via HAMR you may specify a file containing binary data with any name within **$MARS/M.A.R.S./Input**.  
A list of the available input parameters is given in the section **_Inputs_**.  

####----Running on a local machine----  
To perform a simulation locally, run the provided bash script **$MARS/M.A.R.S./Run_MARS.sh**.  
This will create a copy of the:  
- Source code  
- Input files  
- Makefile  
- Bash script  
These copies will be placed within **$MARS/M.A.R.S./simulations/$SIMULATION_FOLDER**.  
**$SIMULATION_FOLDER** will be contain a date and time stamp allowing you to keep track of multiple local running simulations.  
(e.g. *Simulation_2018_08_28_Time_11:36*)  

####----Running on a cluster----  
For use on a comupting cluster a qsub file is required.  
This qsub file should request the desired resources required for your cluster.  
The qsub file should be within **$MARS/M.A.R.S.** and named **_Submit_MARS.qsub_**.  
To submit the job to the cluster you should run the provided bash script **_Run_MARS_remotely.sh_**.  
This script acts almost identically to **Run_MARS.sh** however it calls the qsub file instead of running the executable.

###Inputs  
The Inputs are divided into multiple sections relating to their uses, these sections cover:   
- Simulation type and RNG seed  
- System structure  
- Materials  
- Experiement parameters  
- Solver settings  

**NB:** *# and ; are used to create comments. Anything written on the same line after these symbols is ignored.*  
**NB:** *Letter case type is ignored in the config file, so "S" is identical to "s".*  

===================================================================================================  
There a five input types available:  
- ```String```  
- ```Double```  
- ```Integer```  
- ```Vector Double```  
- ```Vector Integer```  
 
The Vector types require specific formatting: {#,#,#} where # is either a double or an integer.  
The required value type for each input is shown below, **however it should *NOT* be included in the config files.**   
===================================================================================================  
####--MARS_input.cfg-- 
----------SIMULATION SETTINGS----------  
**Sim:Type ```String```** This sets the desired simulation. Currently the options are:  
- *hamr*  
- *hamr\_write\_local*  
- *hamr\_write\_data*  
- *system_gen*  
- *sigma_tc*  
- *Test*  

T**est:Type ```String```** Only used if Sim:Type is set as test. Current options are:  
- *llg_analytical* --> Runs LLG test to provide an analytically solveable solution.  
- *llg_hysteresis* --> Uses LLG to obtain Stoner-Wohlfarth hysteresis profiles.  
- *llg_boltzmann* --> Uses LLG to simulate a system at thermal equilibrium and outputs the corresponding Boltzmann distribution.  
- *llb_mvt* --> Performs an Magnetisation Vs. Temperature simulation using the LLB solver.  
  
**Sim:SEED ```Integer```** Sets the seed number for the random number generators.  

----------SYSTEM STRUCTURE SETTINGS----------  
**Struct:Dim-X ```Double```** System's x-dimension in nm.  
**Struct:Dim-Y ```Double```** System's y-dimension in nm.  
**Struct:Num_Layers ```Integer```** Number of layer in the system.  
**Struct:Avg_Grain_width ```Double```** Average width of a grain in nm.  
**Struct:Std_Dev_Grain_Vol ```Double```** Degree of randomness in Grain sizes.  
**Struct:Packing_Fraction ```Double```** Proportion of system area filled by grains. Used to provide grain spacing.  
**Struct:Interaction_radius ```Double```** Range of the magnetostatic interactions, in units of average grain width.  
**Struct:Magnetostatics ```String```** Flag to set whether the W-matrix is to be imported or determined by MARS, options are "import" OR "dipole".  

----------MATERIAL SETTINGS----------  
**_ * represents the material number._**  
*Materials must be numbered in ascending order, **starting from 1**, there is no upper limit to the number of materials.   
However, if there are less materials listed than requested number of layers the program will halt.*  

**Mat\*:File ```String```** Filename of the material

----------EXPERIMENT SETTINGS----------  
**HAMR:Temp_min ```Double```** Minimum temperature for HAMR simulation, in K.  
**HAMR:Temp_max ```Double```** Maximum temperture of temperture profile for HAMR simulation, in K.  
**HAMR:Cooling_time ```Double```** Rate of temperature change in profile, in seconds. Max temperature occurs at 3\*Cooling_time.  
**HAMR:Applied_field_unit ```Vector Double```** Applied field versor - *Magnitude **MUST** be unity.*  
**HAMR:Applied_field_minimum ```Double```** Minimum applied field strenght, in Oe.  
**HAMR:Applied_field_Strength ```Double```** Maximum applied field strength for field profile, in Oe.  
**HAMR:Field_ramp_time ```Double```** Time required for field to switch from Max to Min and vice versa, in seconds.  
**HAMR:Measurement_time ```Double```** Time step for output of simulation data, in seconds.  
**HAMR:Run_time ```Double```** Total experiment run time, in seconds. ***NOTE:** When performing HAMR_write_data the runtime provides only an upper limit on the simulation time. The simuation will always run long enough to write the number of bits requested.*  
  
#####*Required for HAMR\_write\_data and HAMR\_write\_local.*
**HAMR:Beam_X_width ```Double```** Width of applied laser beam in x.  
**HAMR:Beam_Y_width ```Double```** Width of applied laser beam in y.  
**HAMR:Field_X_width ```Double```** Width of applied field in x.  
**HAMR:Field_Y_width ```Double```** Width of applied field in y.  


#####*Required for HAMR\_write\_data only.*
**HAMR:Bit_width ```Double```** Size of bit in read/write direction (x-dir).  
**HAMR:Bit_length ```Double```** Size of bit perpendicular to read/write direction (y-dir).  
**HAMR:Bit_spacing_X ```Double```** Space between bits in x.  
**HAMR:Bit_spacing_Y ```Double```** Space between bits in y.  
**HAMR:Bit_number ```Integer```** Total number of bits to write.  
**HAMR:Data_Binary ```String```** Type of data to write. Either *square-wave* OR *binary*.  
**HAMR:Data_Location ```String```** Location of data file for a *Binary* write.  

----------KMC SOLVER SETTINGS----------  
**KMC:dt ```Double```** Solver timestep in seconds.
**KMC:Zero_Kelvin_Input ```Double```** Set MARS to apply thermal dependencies on parameters.

----------LLB SOLVER SETTINGS----------  
**LLB:dt ```Double```** Solver timestep in seconds.  

----------LLG SOLVER SETTINGS----------  
**LLG:dt ```Double```** Solver timestep in seconds.  

####--Material files--  

**Mat:Type ```String```** Type of material (e.g. ferromagnetic) -- *Currently unused.*  
**Mat:Initial_mag_type ```String```** Form of initial magnetisation. Currently the options are *Random* or *assigned*.
**Mat:Initial_mag ```Vector Double```** Initial magnetisation direction. - *Magnitude should never exceed unity.*  
**Mat:Easy_axis_Polar ```Double```** Initial polar angle for direction of uniaxial easy axis in degrees.  
**Mat:Easy_axis_Azimuth ```Double```**  Initial azimuthal angle for direction of uniaxial easy axis in degrees.  
**Mat:Anis_angle ```Double```** Dispersion in anisotropy direction in degrees.  
**Mat:thickness ```Double```** Material thickness in nm.  
**Mat:Ms ```Double```** Saturation magnetisation in emu/cc.  
**Mat:Tc_Dist_type ```String```** Tc distribution type - "normal" OR "log-normal".  
**Mat:Avg_Tc ```Double```** Mean Tc value in K.  
**Mat:StdDev_Tc ```Double```** Standard deviation in Tc in K.  
**Mat:K_Dist_type ```String```** Anisotropy distribution type - "normal" OR "log-normal".  
**Mat:Avg_K ```Double```** Mean Anisotropy in erg/cc.  
**Mat:StdDev_K ```Double```** Standard deviation in Anisotropy in erg/cc.  
**Mat:Callen_power ```Double```** Exponent required for the callen-callen scalling.  
**Mat:J_dist_Type ```String```** Exchange distribution type - "normal" OR "log-normal".  
**Mat:StdDev_J ```Double```** Standard deviation in Exchange.  
**Mat:H_sat ```Double```** Exchange field saturation in Oe.  
**Mat:Hexch_out_of_layer_UP ```Double```** Exchange field between neighbouring layers from lower layer to upper layer in Oe.  
**Mat:Hexch_out_of_layer_DOWN ```Double```** Exchange field between neighbouring layers from upper layer to lower layer in Oe. *This is not required for material 1.*  
**Mat:Alpha ```Double```** Damping for LLB/LLG per layer.  
**Mat:Critical_Exponent ```Double```** Exponent for M(T) fitting function.  
